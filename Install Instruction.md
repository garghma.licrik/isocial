# Install ISocial

## Need install:
### JRE 8
### MySql
### TomCat

## Set data in DataBase:
- Open directory with database scripts and open *InstallBD.bat* for editing;
- Set user after *-u* and password after *-p*;
- Save change and run file;

## Deploy project in TomCat
### Without using *Install.bat*
- Coppy file *contollers/target/isocial.war* to *tomcat/webapps*
- run Tomcat *bin/startup.bat*

### With *Install.bat*
- Open for editing and set path to tomcat in *tomcatPath*
- Save change and run