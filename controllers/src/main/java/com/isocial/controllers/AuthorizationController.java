package com.isocial.controllers;

import com.isocial.controllers.utils.mapper.impl.UserMapper;
import com.isocial.dto.AuthorizationDto;
import com.isocial.dto.UserDto;
import com.isocial.models.User;
import com.isocial.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@RestController
public class AuthorizationController {
    private final UserService userService;
    private final UserMapper userMapper;

    @Autowired
    public AuthorizationController(UserService userService, UserMapper userMapper) {
        this.userService = userService;
        this.userMapper = userMapper;
    }

    @PostMapping("/login")
    public ResponseEntity<?> authorization(@RequestBody AuthorizationDto dto) {
        User currentUser = userService.foundByUsername(dto.getUsername());
        if (currentUser == null) {
            return new ResponseEntity<>("User not found", HttpStatus.NOT_ACCEPTABLE);
        }
        if (userService.authorization(currentUser.getPassword(), dto.getPassword())) {
            return ResponseEntity.ok(userService.createToken(currentUser));
        }

        return new ResponseEntity<>("Bad password", HttpStatus.FORBIDDEN);
    }

    @GetMapping("/registration")
    public ResponseEntity<?> registration(@RequestBody UserDto userDto) {
        if (userService.foundByUsername(userDto.getUsername()) != null) {
            return new ResponseEntity<>("Login is created. Please change login", HttpStatus.NO_CONTENT);
        }
        User user = userMapper.toEntity(userDto);
        userService.registration(user);

        return ResponseEntity.ok(userService.createToken(user));
    }

    @GetMapping("/setting")
    public UserDto getAllInformation(Principal principal) {
        return userMapper.toDto(userService.getWithAllInformation(principal.getName()));
    }

    @PutMapping("/setting")
    public UserDto setting(@RequestBody UserDto userDto) {
        return userMapper.toDto(userService.updateUser(userMapper.toEntity(userDto)));
    }
}