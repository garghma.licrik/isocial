package com.isocial.controllers;

import com.isocial.controllers.utils.mapper.impl.CommunityMapper;
import com.isocial.controllers.utils.mapper.impl.CommunityMemberMapper;
import com.isocial.controllers.utils.mapper.impl.PostMapper;
import com.isocial.dto.*;
import com.isocial.models.Community;
import com.isocial.models.CommunityMember;
import com.isocial.models.User;
import com.isocial.services.CommunityService;
import com.isocial.services.UserService;
import com.isocial.services.utils.CommunityAction;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/communities")
public class CommunityController {
    private final UserService userService;
    private final CommunityService communityService;
    private final CommunityMapper communityMapper;
    private final PostMapper postMapper;
    private final CommunityMemberMapper communityMemberMapper;

    public CommunityController(UserService userService,
                               CommunityService communityService,
                               CommunityMapper communityMapper,
                               PostMapper postMapper,
                               CommunityMemberMapper communityMemberMapper) {
        this.userService = userService;
        this.communityService = communityService;
        this.communityMapper = communityMapper;
        this.postMapper = postMapper;
        this.communityMemberMapper = communityMemberMapper;
    }

    @GetMapping
    public List<CommunityDto> getAllUserCommunities(Principal principal) {
        List<Community> list =  communityService.getUserCommunities(
                userService.getWithCommunities(principal.getName())
        );

        return list.stream()
                .map(communityMapper::toDto)
                .collect(Collectors.toList());
    }

    @GetMapping(value = "/find")
    public List<CommunityDto> getAllCommunities() {
        List<Community> list = communityService.getAllCommunities();

        return list.stream()
                .map(communityMapper::toDto)
                .collect(Collectors.toList());
    }

    @GetMapping(value = "/find", params = {"name"})
    public CommunityDto filterCommunities(@RequestParam String name) {
        return communityMapper.toDto(communityService.findCommunitiesByName(name));
    }

    @GetMapping(value = "/{id}", params = {"action"})
    public CommunityDto communityAction(@PathVariable Long id,
                                        @RequestParam String action,
                                        Principal principal) {
        User user = userService.foundByUsername(principal.getName());

        return communityMapper.toDto(communityService
                .actionCommunity(user, id, CommunityAction.get(action)));
    }

    @GetMapping("/{id}")
    public CommunityDto getCommunity(@PathVariable Long id) {
        Community community = communityService.findCommunityById(id);

        return communityMapper.toDto(community);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> removeCommunity(@PathVariable Long id, Principal principal) {
        communityService.removeCommunity(principal.getName(), id);

        return ResponseEntity.ok(id + " deleted");
    }

    @PostMapping("/{id}")
    public CommunityDto createPost(@PathVariable Long id,
                                   @RequestBody PostDto postDto,
                                   Principal principal) {
        return communityMapper.toDto(communityService
                .createPost(id, principal.getName(), postMapper.toEntity(postDto)));
    }

    @DeleteMapping(value = "/{id}", params = {"idPost"})
    public CommunityDto deletePost(@PathVariable Long id,
                                   @RequestParam Long idPost,
                                   Principal principal) {
        return communityMapper.toDto(communityService
                .removePost(id, principal.getName(), idPost));
    }

    @PutMapping(value = "/{id}")
    public CommunityDto updatePost(@PathVariable Long id,
                                   @RequestBody PostDto postDto,
                                   Principal principal) {
        return communityMapper.toDto(communityService
                .updatePost(id, principal.getName(), postMapper.toEntity(postDto)));
    }

    @PostMapping
    public CommunityDto createCommunity(@RequestBody CommunityDto communityDto, Principal principal) {
        User currentUser = userService.foundByUsername(principal.getName());

        return communityMapper.toDto(communityService
                .createCommunity(communityMapper.toEntity(communityDto), currentUser));
    }

    @GetMapping("/{id}/members")
    public List<CommunityMemberDto> getCommunityMembers(@PathVariable Long id) {
        List<CommunityMember> members = communityService.getCommunityMembers(id);

        return members.stream()
                .map(communityMemberMapper::toDto)
                .collect(Collectors.toList());
    }
}