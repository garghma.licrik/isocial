package com.isocial.controllers;

import com.isocial.controllers.utils.mapper.impl.DialogMapper;
import com.isocial.controllers.utils.mapper.impl.MessageMapper;
import com.isocial.dto.DialogDto;
import com.isocial.dto.MessageDto;
import com.isocial.models.Dialog;
import com.isocial.models.User;
import com.isocial.services.DialogService;
import com.isocial.services.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class DialogController {
    private final UserService userService;
    private final DialogService dialogService;
    private final DialogMapper dialogMapper;
    private final MessageMapper messageMapper;

    public DialogController(UserService userService,
                            DialogService dialogService,
                            DialogMapper dialogMapper,
                            MessageMapper messageMapper) {
        this.userService = userService;
        this.dialogService = dialogService;
        this.dialogMapper = dialogMapper;
        this.messageMapper = messageMapper;
    }

    @GetMapping("/dialogs")
    public List<DialogDto> getUserDialogs(Principal principal) {
        List<Dialog> list = dialogService.getAllUserDialogs(userService.getWithDialogs(principal.getName()));

        return list.stream()
                .map(dialogMapper::toDto)
                .collect(Collectors.toList());
    }

    @GetMapping(value = "/dialogs/{id}")
    public DialogDto getDialogById(@PathVariable long id, Principal principal) {
        return dialogMapper.toDto(dialogService.decodeDialog(
                dialogService.getDialogById(id, principal.getName())));
    }

    @DeleteMapping("/dialogs")
    public ResponseEntity<?> deleteDialog(@RequestBody DialogDto dialogDto, Principal principal) {
        dialogService.removeDialog(dialogMapper.toEntity(dialogDto), principal.getName());

        return ResponseEntity.ok("Delete dialog");
    }

    @PostMapping(value = "/dialogs/{id}")
    public DialogDto createMessage(@PathVariable Long id, @RequestBody MessageDto messageDto, Principal principal) {
        Dialog dialog = dialogService.createMessage(id, messageMapper.toEntity(messageDto),
                userService.foundByUsername(principal.getName()));

        return dialogMapper.toDto(dialogService.decodeDialog(dialog));
    }

    @PutMapping(value = "/dialogs/{id}")
    public DialogDto updateMessage(@PathVariable Long id, @RequestBody MessageDto messageDto, Principal principal) {
        Dialog dialog = dialogService.updateMessage(id, messageMapper.toEntity(messageDto), principal.getName());

        return dialogMapper.toDto(dialogService.decodeDialog(dialog));
    }

    @DeleteMapping(value = "/dialogs/{id}")
    public DialogDto deleteMessage(@PathVariable Long id, @RequestBody MessageDto message, Principal principal) {
        Dialog dialog = dialogService.removeMessage(id, messageMapper.toEntity(message), principal.getName());

        return dialogMapper.toDto(dialogService.decodeDialog(dialog));
    }

    @PostMapping("/users/{username}/dialog")
    public DialogDto createNewDialog(@PathVariable String username, @RequestBody DialogDto dto, Principal principal) {
        User currentUser = userService.getWithDialogs(principal.getName());
        User targetUser = userService.getWithDialogs(username);

        return dialogMapper.toDto(dialogService.createDialog(currentUser, targetUser, dto.getName()));
    }
}