package com.isocial.controllers;

import com.isocial.services.utils.FriendsSection;
import com.isocial.controllers.utils.mapper.impl.FriendMapper;
import com.isocial.dto.FriendDto;
import com.isocial.models.User;
import com.isocial.services.FriendService;
import com.isocial.services.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class FriendController {
    private final UserService userService;
    private final FriendService friendService;
    private final FriendMapper friendMapper;

    public FriendController(UserService userService, FriendService friendService, FriendMapper friendMapper) {
        this.userService = userService;
        this.friendService = friendService;
        this.friendMapper = friendMapper;
    }

    @GetMapping("/friends")
    public List<FriendDto> getActiveFriends(Principal principal) {
        return friendService.getActiveFriends(principal.getName())
                .stream()
                .map(friendMapper::toDto)
                .collect(Collectors.toList());
    }

    @GetMapping(value = "/friends", params = {"section"})
    public List<FriendDto> getFriendRequests(@RequestParam(name = "section") String section, Principal principal) {
        return friendService.getFriendsBySection(principal.getName(), FriendsSection.get(section))
                .stream()
                .map(friendMapper::toDto)
                .collect(Collectors.toList());
    }

    @DeleteMapping(value = {"/friends"}, params = {"id"})
    public ResponseEntity<?> deleteFriendRequest(@RequestParam Long id) {
        friendService.deleteFriend(id);

        return ResponseEntity.ok("Delete from friends");
    }

    @PutMapping(value = {"/friends"}, params = {"id"})
    public ResponseEntity<?> acceptFriendRequest(@RequestParam Long id) {
        friendService.acceptFriendRequest(id);

        return ResponseEntity.ok("Accept request for friends");
    }

    @GetMapping("/users/{username}/friends")
    public List<FriendDto> getUserFriends(@PathVariable String username) {
        return friendService.getActiveFriends(username)
                .stream()
                .map(friendMapper::toDto)
                .collect(Collectors.toList());
    }

    @PostMapping(value = "/users/{username}", params = {"action"})
    public ResponseEntity<?> createFriendRequest(@PathVariable String username,
                                                 @RequestParam String action,
                                                 Principal principal) {
        if (action.equals("friend")) {
            User currentUser = userService.foundByUsername(principal.getName());
            User toUser = userService.foundByUsername(username);
            friendService.createFriendRequest(currentUser, toUser);

            return ResponseEntity.ok("Send request to " + username);
        }

        return new ResponseEntity<>("Problem with operation", HttpStatus.BAD_GATEWAY);
    }
}