package com.isocial.controllers;

import com.isocial.dto.HobbyDto;
import com.isocial.models.Hobby;
import com.isocial.services.HobbyService;
import org.modelmapper.ModelMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/hobbies")
public class HobbyController {
    private final ModelMapper mapper;
    private final HobbyService hobbyService;

    public HobbyController(ModelMapper mapper, HobbyService hobbyService) {
        this.mapper = mapper;
        this.hobbyService = hobbyService;
    }

    @GetMapping
    public List<HobbyDto> getAllHobbies() {
        return hobbyService.getAllHobbies().stream().map(x -> mapper.map(x, HobbyDto.class)).collect(Collectors.toList());
    }

    @PostMapping
    public ResponseEntity<?> createNewHobby(@RequestBody HobbyDto hobbyDto) {
        hobbyService.insertNewHobby(mapper.map(hobbyDto, Hobby.class));

        return ResponseEntity.ok("Create new hobby");
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> removeHobby(@PathVariable Long id) {
        hobbyService.removeById(id);

        return ResponseEntity.ok("Remove");
    }

    @PutMapping("/{id}")
    public HobbyDto updateHobby(@PathVariable Long id, @RequestBody HobbyDto hobby) {
        return mapper.map(hobbyService.updateHobby(id, mapper.map(hobby, Hobby.class)), HobbyDto.class);
    }

    @GetMapping("/{id}")
    public HobbyDto getHobbyById(@PathVariable Long id) {
        return mapper.map(hobbyService.getHobbyById(id), HobbyDto.class);
    }
}