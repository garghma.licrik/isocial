package com.isocial.controllers;

import com.isocial.controllers.utils.mapper.impl.PostMapper;
import com.isocial.controllers.utils.mapper.impl.UserMapper;
import com.isocial.dto.PostDto;
import com.isocial.dto.UserDto;
import com.isocial.models.User;
import com.isocial.services.UserService;
import com.isocial.services.exeption.ServiceException;
import com.isocial.services.utils.UserFoundOption;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/users")
public class UserController {
    private final UserService userService;
    private final UserMapper userMapper;
    private final PostMapper postMapper;

    public UserController(UserService userService, UserMapper userMapper, PostMapper postMapper) {
        this.userService = userService;
        this.userMapper = userMapper;
        this.postMapper = postMapper;
    }

    @GetMapping
    public List<UserDto> getAllUsers() {
        List<User> users = userService.getAllUsers();

        return converterList(users);
    }

    @GetMapping("/find")
    public List<UserDto> foundUsersInUsersCity(@RequestBody UserFoundOption userFoundOption) {
        List<User> users = userService.findUsers(userFoundOption);

        return converterList(users);
    }

    @GetMapping("/{username}")
    public UserDto getUserByUsername(@PathVariable String username) {
        User user = userService.foundByUsername(username);

        return userMapper.toDto(user);
    }

    @PostMapping("/{username}")
    public UserDto createPost(@PathVariable String username,
                              @RequestBody PostDto postDto,
                              Principal principal) {
        if (username.equals(principal.getName())) {
            return userMapper.toDto(userService.createPost(username, postMapper.toEntity(postDto)));
        } else {
            throw new ServiceException("No permission");
        }
    }

    @PutMapping(value = "/{username}")
    public UserDto updatePost(@PathVariable String username,
                              @RequestBody PostDto postDto,
                              Principal principal) {
        if (username.equals(principal.getName())) {
            return userMapper.toDto(userService.updatePost(username, postMapper.toEntity(postDto)));
        } else {
            throw new ServiceException("No permission");
        }
    }

    @DeleteMapping(value = "/{username}")
    public UserDto deletePost(@PathVariable String username,
                              @RequestBody PostDto postDto,
                              Principal principal) {
        if (username.equals(principal.getName())) {
            return userMapper.toDto(userService.removePost(username, postMapper.toEntity(postDto)));
        } else {
            throw new ServiceException("No permission");
        }
    }

    private List<UserDto> converterList(List<User> users) {
        return users.stream()
                .map(userMapper::toDto)
                .collect(Collectors.toList());
    }
}