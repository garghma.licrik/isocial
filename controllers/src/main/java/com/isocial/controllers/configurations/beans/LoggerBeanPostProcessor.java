package com.isocial.controllers.configurations.beans;

import com.isocial.repositories.annotation.CustomLogger;
import org.apache.logging.log4j.core.util.ReflectionUtil;
import org.springframework.beans.factory.config.BeanPostProcessor;

import java.lang.reflect.Field;
import org.apache.logging.log4j.LogManager;
import org.springframework.stereotype.Component;

@Component
public class LoggerBeanPostProcessor implements BeanPostProcessor {
    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) {
        Field[] fields = bean.getClass().getDeclaredFields();
        for (Field field : fields) {
            CustomLogger annotation = field.getAnnotation(CustomLogger.class);
            if (annotation != null) {
                field.setAccessible(true);
                String name = bean.getClass().getSimpleName();
                ReflectionUtil.setFieldValue(field, bean, LogManager.getLogger(name));
                field.setAccessible(false);
            }
        }
        return bean;
    }
}
