package com.isocial.controllers.utils;

import com.isocial.repositories.exception.RepositoryException;
import com.isocial.services.exeption.ServiceException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.nio.file.AccessDeniedException;

@RestControllerAdvice
public class ControllerAdvisor {
    @ExceptionHandler({RepositoryException.class, ServiceException.class, AccessDeniedException.class, Exception.class})
    public ResponseEntity<?> handleValidationException(Exception validationException) {
        return new ResponseEntity<>(validationException.getMessage(), HttpStatus.NO_CONTENT);
    }
}

