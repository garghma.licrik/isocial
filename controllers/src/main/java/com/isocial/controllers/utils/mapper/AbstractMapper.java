package com.isocial.controllers.utils.mapper;

import com.isocial.dto.BaseDto;
import com.isocial.models.AEntity;
import org.modelmapper.ModelMapper;

import java.util.Objects;

public abstract class AbstractMapper<E extends AEntity, D extends BaseDto> implements BaseMapper<E, D> {
    protected final ModelMapper mapper;

    private final Class<E> entityClass;
    private final Class<D> dtoClass;

    public AbstractMapper(ModelMapper mapper, Class<E> entityClass, Class<D> dtoClass) {
        this.mapper = mapper;
        this.entityClass = entityClass;
        this.dtoClass = dtoClass;
    }

    @Override
    public E toEntity(D dto) {
        return Objects.isNull(dto)
                ? null
                : mapper.map(dto, entityClass);
    }

    @Override
    public D toDto(E entity) {
        return Objects.isNull(entity)
                ? null
                : mapper.map(entity, dtoClass);
    }
}
