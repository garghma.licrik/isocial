package com.isocial.controllers.utils.mapper.impl;

import com.isocial.controllers.utils.mapper.AbstractMapper;
import com.isocial.dto.CommunityDto;
import com.isocial.models.Community;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class CommunityMapper extends AbstractMapper<Community, CommunityDto> {
    CommunityMapper(ModelMapper mapper) {
        super(mapper, Community.class, CommunityDto.class);
    }
}
