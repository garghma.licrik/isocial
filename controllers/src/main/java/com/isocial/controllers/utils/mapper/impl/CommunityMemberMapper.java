package com.isocial.controllers.utils.mapper.impl;

import com.isocial.controllers.utils.mapper.AbstractMapper;
import com.isocial.dto.CommunityMemberDto;
import com.isocial.models.CommunityMember;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class CommunityMemberMapper extends AbstractMapper<CommunityMember, CommunityMemberDto> {
    CommunityMemberMapper(ModelMapper mapper) {
        super(mapper, CommunityMember.class, CommunityMemberDto.class);
    }
}
