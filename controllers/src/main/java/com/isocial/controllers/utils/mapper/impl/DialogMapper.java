package com.isocial.controllers.utils.mapper.impl;

import com.isocial.controllers.utils.mapper.AbstractMapper;
import com.isocial.dto.DialogDto;
import com.isocial.models.Dialog;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class DialogMapper extends AbstractMapper<Dialog, DialogDto> {
    DialogMapper(ModelMapper mapper) {
        super(mapper, Dialog.class, DialogDto.class);
    }
}
