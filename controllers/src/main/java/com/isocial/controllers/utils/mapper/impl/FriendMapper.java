package com.isocial.controllers.utils.mapper.impl;

import com.isocial.controllers.utils.mapper.AbstractMapper;
import com.isocial.dto.FriendDto;
import com.isocial.models.Friend;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class FriendMapper extends AbstractMapper<Friend, FriendDto> {
    FriendMapper(ModelMapper mapper) {
        super(mapper, Friend.class, FriendDto.class);
    }
}
