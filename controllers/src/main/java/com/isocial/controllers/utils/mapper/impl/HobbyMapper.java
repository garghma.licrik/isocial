package com.isocial.controllers.utils.mapper.impl;

import com.isocial.controllers.utils.mapper.AbstractMapper;
import com.isocial.dto.HobbyDto;
import com.isocial.models.Hobby;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class HobbyMapper extends AbstractMapper<Hobby, HobbyDto> {
    HobbyMapper(ModelMapper mapper) {
        super(mapper, Hobby.class, HobbyDto.class);
    }
}
