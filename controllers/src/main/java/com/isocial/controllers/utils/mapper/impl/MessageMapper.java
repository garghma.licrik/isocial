package com.isocial.controllers.utils.mapper.impl;

import com.isocial.controllers.utils.mapper.AbstractMapper;
import com.isocial.dto.MessageDto;
import com.isocial.models.Message;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class MessageMapper extends AbstractMapper<Message, MessageDto> {
    MessageMapper(ModelMapper mapper) {
        super(mapper, Message.class, MessageDto.class);
    }
}
