package com.isocial.controllers.utils.mapper.impl;

import com.isocial.controllers.utils.mapper.AbstractMapper;
import com.isocial.dto.PostDto;
import com.isocial.models.Post;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class PostMapper extends AbstractMapper<Post, PostDto> {
    PostMapper(ModelMapper mapper) {
        super(mapper, Post.class, PostDto.class);
    }
}
