package com.isocial.controllers.utils.mapper.impl;

import com.isocial.controllers.utils.mapper.AbstractMapper;
import com.isocial.dto.UserDto;
import com.isocial.models.User;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.springframework.stereotype.Component;

@Component
public class UserMapper extends AbstractMapper<User, UserDto> {
    public UserMapper(ModelMapper mapper) {
        super(mapper, User.class, UserDto.class);

        this.mapper.getConfiguration().setAmbiguityIgnored(true);
        this.mapper.addMappings(new PropertyMap<User, UserDto>() {
            @Override
            protected void configure() {
                skip(destination.getPassword());
            }
        });
    }
}
