create database isocial;

use isocial;

create table communities(
	id int auto_increment primary key,
    name varchar(150) not null,
    create_date date not null,
    description text
);

create table users(
	id int auto_increment primary key,
    username varchar(150) not null,
    password varchar(150) not null,
    first_name varchar(150) not null,
    second_name varchar(150) not null,
    last_name varchar(150) not null,
    email varchar(150) not null,
    birth_date date,
    city varchar(150) not null,
    last_visit_date datetime,
    gender varchar(10) not null,
    role varchar(6) not null
);

create table posts(
	id int auto_increment primary key,
    message text not null,
    id_user int,
    id_community int, 
    create_date datetime not null,
    likes_count int,
    foreign key (id_user) references users(id),
    foreign key (id_community) references communities(id)
);

create table communities_members(
	id int auto_increment primary key,
    id_community int not null,
    id_user int not null,
    rights varchar(50) not null,
    id_last_read_post int,
    foreign key (id_community) references communities(id),
    foreign key (id_user) references users(id)
);

create table hobbies (
	id int auto_increment primary key,
    name varchar(150) not null
);

create table user_hobbies(
	id int auto_increment primary key,
    id_user int not null,
    id_hobby int not null,
    foreign key (id_user) references users(id),
    foreign key (id_hobby) references hobbies(id)
);

create table friends(
	id int auto_increment primary key,
    id_first_user int not null,
    id_second_user int not null,
    active_state boolean,
    foreign key (id_first_user) references users(id),
    foreign key (id_second_user) references users(id)
);

create table dialogs(
	id int auto_increment primary key,
    name varchar(150),
    create_date datetime
);

create table messages(
	id int auto_increment primary key,
    id_dialog int not null,
    id_user int not null,
    message text not null,
    create_date date not null,
    foreign key (id_dialog) references dialogs(id),
    foreign key (id_user) references users(id)
);

create table dialog_members(
	id int auto_increment primary key,
    id_dialog int not null,
    id_user int not null,
    id_last_read_message int,
    foreign key (id_dialog) references dialogs(id),
    foreign key (id_user) references users(id)
);


create table comments(
	id int auto_increment primary key,
    id_user int not null,
    id_post int not null,
    message text not null,
    create_time datetime not null,
    likes_count int,
    foreign key (id_user) references users(id),
    foreign key (id_post) references posts(id)
);