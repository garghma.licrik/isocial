use isocial;

insert into communities(
	name,
    create_date,
    description
) values
("Test group", "2022-02-20", "test description"),
("admin group", "2019-01-25", "admin description");

insert into users(
	username, 
    password,
    first_name,
    second_name,
    last_name,
    email,
    birth_date,
    city,
    gender,
    role
) values
("admin", "$2a$04$8ZG.n1Bwuk.OclsGcKZ6teAOcAslx5KqJrxjnr.gTNUhW6jF5kK7e", "Petr", "Petrovich", "Petrov", "test@mail.ru", "1995-10-25", "Minsk", "male", "admin"),
("user", "$2a$04$maNnrvn4t2gE3O4BawLO1uMe2W/SUnxyl0PuoSNyN8D2j/MFmR/Te", "Ivan", "Ivanov", "Ivanovich", "ivanov@mail.ru", "1989-02-20", "Moskov", "male", "user"),
("test", "$2a$04$OIRPgSs1uz/0D6jYpOn7DOWcrLz/z1nIGGGjplShvlUXeGpBkNLZG", "Ferd", "Ferd", "Ferd", "ivanov@mail.ru", "1989-02-20", "Minsk", "male", "user");

insert into hobbies(
	name
) values 
("sport"),
("sinema"),
("films");

insert into user_hobbies(
	id_user,
    id_hobby
) values 
(1, 1), (1, 3), (2, 2);

insert into friends(
	id_first_user,
    id_second_user,
    active_state
) values (1, 2, false);

insert into dialogs(
	name,
    create_date
) values ("test dialog", "2022-02-22 19:00:20");

insert into dialog_members(
	id_dialog,
    id_user
) values (1, 2), (1, 1);

insert into messages(
	id_dialog,
    id_user,
    message,
    create_date
) values 
(1, 2, "Message TEst", "2022-05-19 21:48:30"), 
(1, 1, "Message", "2022-05-19 21:48:31");

insert into communities_members(
	id_community,
    id_user,
    rights
) values
(1, 2, "admin");

insert into posts(
	message, 
    id_user,
    id_community,
    create_date
) values
("Test message", null, 1, "2022-05-19 22:10:30");

insert into comments(
	id_user,
    id_post,
    message,
    create_time
) values 
(2, 1, "Best post", "2022-05-19 21:02:10");