package com.isocial.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AuthorizationDto {
    @NotBlank(message = "Username may not be null")
    private String username;
    @NotBlank(message = "Password may not be null")
    private String password;
}
