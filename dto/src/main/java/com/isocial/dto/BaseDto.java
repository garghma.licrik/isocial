package com.isocial.dto;

import lombok.Data;

@Data
public class BaseDto {
    private Long id;
}