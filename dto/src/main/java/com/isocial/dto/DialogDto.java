package com.isocial.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DialogDto extends BaseDto {
    private String name;
    private LocalDateTime createTime;
    private List<DialogMemberDto> members;
    private List<MessageDto> messages;
}
