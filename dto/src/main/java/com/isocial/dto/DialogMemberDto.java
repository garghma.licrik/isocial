package com.isocial.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DialogMemberDto extends BaseDto {
    private UserDto user;
    private Long lastReadMessage;
}
