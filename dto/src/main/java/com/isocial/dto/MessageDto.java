package com.isocial.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MessageDto extends BaseDto {
    private UserDto user;
    @NotBlank(message = "Need write message")
    private String message;
    private LocalDateTime dateTime;
    private boolean watched;
}
