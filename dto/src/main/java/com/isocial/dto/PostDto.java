package com.isocial.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PostDto extends BaseDto {
    @NotBlank(message = "Write message")
    private String message;
    private LocalDateTime createTime;
    private Long countLike;
}