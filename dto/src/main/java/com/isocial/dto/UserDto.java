package com.isocial.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserDto extends BaseDto {
    @NotBlank
    private String username;
    @NotBlank
    private String firstName;
    @NotBlank
    private String secondName;
    @NotBlank
    private String lastName;
    @NotBlank
    private String email;
    @NotBlank
    private LocalDate birthDay;
    @NotBlank
    private String city;
    private LocalDateTime lastVisitDate;
    @NotBlank
    private String gender;
    @NotBlank
    private String role;
    @NotBlank
    private String password;

    private List<PostDto> posts;
    private List<HobbyDto> hobbies;
}
