package com.isocial.models;

import lombok.Data;

import javax.persistence.*;

@MappedSuperclass
@Data
public abstract class AEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;
}
