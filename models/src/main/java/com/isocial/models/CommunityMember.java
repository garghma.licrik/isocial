package com.isocial.models;

import com.isocial.models.utils.Role;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "communities_members")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CommunityMember extends AEntity {
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_community", nullable = false)
    private Community community;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_user", nullable = false)
    private User user;

    @Column(name = "rights", nullable = false)
    @Enumerated(EnumType.STRING)
    private Role role;

    @Column(name = "id_last_read_post")
    private Long lastPost;
}
