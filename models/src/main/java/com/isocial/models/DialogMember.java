package com.isocial.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "dialog_members")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DialogMember extends AEntity {
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_dialog", nullable = false)
    private Dialog dialog;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_user", nullable = false)
    private User user;

    @Column(name  ="id_last_read_message")
    private Long lastMessage;
}
