package com.isocial.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "hobbies")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Hobby extends AEntity {
    @Column(name = "name")
    private String name;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "user_hobbies",
            joinColumns = @JoinColumn(name = "id_hobby"),
            inverseJoinColumns = @JoinColumn(name = "id_user")
    )
    private List<User> users;
}