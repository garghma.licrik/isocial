package com.isocial.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "messages")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Message extends AEntity {
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_dialog", nullable = false)
    private Dialog dialog;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_user", nullable = false)
    private User user;
    @Column(name = "message")
    private String message;
    @Column(name = "create_date")
    private LocalDateTime dateTime;
    @Transient
    private boolean watched = true;
}

