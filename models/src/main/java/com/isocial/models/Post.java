package com.isocial.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "posts")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Post extends AEntity {
    @Column(name = "message")
    private String message;

    @Column(name = "create_date")
    private LocalDateTime createTime;

    @Column(name = "likes_count")
    private Long countLike;

    @ManyToOne
    @JoinColumn(name = "id_user")
    private User user;

   @ManyToOne
   @JoinColumn(name = "id_community")
   private Community community;
}