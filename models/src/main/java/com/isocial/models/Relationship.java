package com.isocial.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "friends")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Relationship extends AEntity {
    @JoinColumn(name = "id_first_user")
    @ManyToOne
    private User fromUser;
    @JoinColumn(name = "id_second_user")
    @ManyToOne
    private User toUser;
    @Column(name = "active_state")
    private boolean state;
}