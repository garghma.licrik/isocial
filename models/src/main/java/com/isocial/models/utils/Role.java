package com.isocial.models.utils;

public enum Role {
    ADMIN,
    USER
}
