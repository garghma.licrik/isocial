package com.isocial.models.utils;

public enum UserGender {
    MALE,
    FEMALE;
}
