package com.isocial.repositories;

import com.isocial.repositories.exception.RepositoryException;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;
import java.util.Map;

public abstract class AbstractBaseRepository<E> implements BaseRepository<Long, E> {
    protected static final String PARAMETER_ID = "id";
    protected static final String PARAMETER_NAME = "name";
    protected static final String PARAMETER_STATE = "state";

    private final Class<E> type;
    @PersistenceContext
    private EntityManager manager;

    public AbstractBaseRepository(Class<E> type) {
        this.type = type;
    }

    protected List<E> findEntitiesByQuery(String query) {
        try {
            return manager.createQuery(query, type).getResultList();
        } catch (Exception ex) {
            setErrorMessage(ex.getMessage());
            throw new RepositoryException(ex.getMessage());
        }
    }

    protected List<E> findEntitiesWithParam(String queryString, Map<String, Object> param) {
        try {
            TypedQuery<E> query = manager.createQuery(queryString, type);

            for(Map.Entry<String, Object> item : param.entrySet()) {
                query.setParameter(item.getKey(), item.getValue());
            }

            return query.getResultList();
        } catch (Exception ex) {
            setErrorMessage(ex.getMessage());
            throw new RepositoryException(ex.getMessage());
        }
    }

    protected E findEntityByQuery(String query) {
        try {
            return manager.createQuery(query, type).getSingleResult();
        } catch (Exception ex) {
            setErrorMessage(ex.getMessage());
            throw new RepositoryException(ex.getMessage());
        }
    }

    protected E findEntityWithParam(String queryString, Map<String, Object> param) {
        try {
            TypedQuery<E> query = manager.createQuery(queryString, type);

            for(Map.Entry<String, Object> item : param.entrySet()) {
                query.setParameter(item.getKey(), item.getValue());
            }

            return query.getSingleResult();
        } catch (Exception ex) {
            setErrorMessage(ex.getMessage());
            throw new RepositoryException(ex.getMessage());
        }
    }

    @Override
    public E findById(Long id) {
        try {
            return manager.find(type, id);
        } catch (Exception ex) {
            setErrorMessage(ex.getMessage());
            throw new RepositoryException(ex.getMessage());
        }
    }

    @Override
    public E update(E entity) {
        try {
            return manager.merge(entity);
        } catch (Exception ex) {
            setErrorMessage(ex.getMessage());
            throw new RepositoryException(ex.getMessage());
        }
    }

    @Override
    public void insert(E entity) {
        try {
            manager.persist(entity);
        } catch (Exception ex) {
            setErrorMessage(ex.getMessage());
            throw new RepositoryException(ex.getMessage());
        }
    }

    @Override
    public void delete(E entity) {
        try {
            manager.remove(entity);
        } catch (Exception ex) {
            setErrorMessage(ex.getMessage());
        }
    }

    abstract protected void setErrorMessage(String message);
}
