package com.isocial.repositories;

import java.util.List;

public interface BaseRepository<L, E> {
    List<E> findAll();
    E findById(L id);
    E update(E entity);
    void insert(E entity);
    void delete(E entity);
}
