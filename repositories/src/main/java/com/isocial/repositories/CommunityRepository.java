package com.isocial.repositories;

import com.isocial.models.Community;

import java.util.List;

public interface CommunityRepository extends BaseRepository<Long, Community> {
    Community getWithPostsById(Long id);
    Community getWithMembersById(Long id);
    Community foundByName(String name);
    List<Community> findByKeywords(String keywords);
}
