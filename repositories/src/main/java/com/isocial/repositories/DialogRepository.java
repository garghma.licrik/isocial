package com.isocial.repositories;

import com.isocial.models.Dialog;

import java.util.List;

public interface DialogRepository extends BaseRepository<Long, Dialog> {
    List<Dialog> findByUserId(Long id);
    Dialog getDialogWithMessage(Long id);
}
