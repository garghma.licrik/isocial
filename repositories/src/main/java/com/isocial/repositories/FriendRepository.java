package com.isocial.repositories;

import com.isocial.models.Relationship;

import java.util.List;

public interface FriendRepository extends BaseRepository<Long, Relationship> {
    List<Relationship> findFriendsFromUserByState(String username, boolean state);
    List<Relationship> findFriendsToUserByState(String username, boolean state);
}
