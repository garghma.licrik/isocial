package com.isocial.repositories;

import com.isocial.models.Hobby;

public interface HobbyRepository extends BaseRepository<Long, Hobby> {
    Hobby findByName(String name);
}
