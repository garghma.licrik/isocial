package com.isocial.repositories;

import com.isocial.models.User;

import java.util.List;

public interface UserRepository extends BaseRepository<Long, User> {
    User foundByUsername(String username);
    User foundByUsernameWithPosts(String username);
    User foundByUsernameWithHobbies(String username);
    User foundByUsernameWithDialogs(String username);
    User foundByUsernameWithCommunities(String username);
    List<User> foundByCity(String city);
    List<User> foundByHobbies(Long hobbies);
}
