package com.isocial.repositories.impl;

import com.isocial.models.Community;
import com.isocial.repositories.AbstractBaseRepository;
import com.isocial.repositories.CommunityRepository;
import com.isocial.repositories.annotation.CustomLogger;
import org.springframework.stereotype.Repository;
import org.apache.logging.log4j.Logger;

import java.util.HashMap;
import java.util.List;

@Repository
public class CommunityRepositoryImpl extends AbstractBaseRepository<Community> implements CommunityRepository {
    @CustomLogger
    private Logger logger;
    private static final String SELECT_ALL = "from Community";
    private static final String SELECT_WITH_POSTS = "SELECT community FROM Community community JOIN FETCH community.posts posts where community.id = :" + PARAMETER_ID;
    private static final String SELECT_WITH_MEMBERS = "SELECT community FROM Community community JOIN FETCH community.members members where community.id = :" + PARAMETER_ID;
    private static final String SELECT_BY_NAME = "from Community where name = :" + PARAMETER_NAME;
    private static final String SELECT_BY_KEYWORDS = "from Community where description CONTAINS :" + PARAMETER_NAME;

    public CommunityRepositoryImpl() {
        super(Community.class);
    }

    @Override
    protected void setErrorMessage(String message) {
        logger.error(message);
    }

    @Override
    public Community getWithPostsById(Long id) {
        HashMap<String, Object> map = new HashMap<>();
        map.put(PARAMETER_ID, id);

        return super.findEntityWithParam(SELECT_WITH_POSTS, map);
    }

    @Override
    public Community getWithMembersById(Long id) {
        HashMap<String, Object> map = new HashMap<>();
        map.put(PARAMETER_ID, id);

        return super.findEntityWithParam(SELECT_WITH_MEMBERS, map);
    }

    @Override
    public Community foundByName(String name) {
        HashMap<String, Object> map = new HashMap<>();
        map.put(PARAMETER_NAME, name);

        return super.findEntityWithParam(SELECT_BY_NAME, map);
    }

    @Override
    public List<Community> findByKeywords(String keywords) {
        HashMap<String, Object> map = new HashMap<>();
        map.put(PARAMETER_NAME, keywords);

        return super.findEntitiesWithParam(SELECT_BY_KEYWORDS, map);
    }

    @Override
    public List<Community> findAll() {
        return super.findEntitiesByQuery(SELECT_ALL);
    }
}
