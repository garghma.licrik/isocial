package com.isocial.repositories.impl;

import com.isocial.models.Dialog;
import com.isocial.repositories.AbstractBaseRepository;
import com.isocial.repositories.DialogRepository;
import com.isocial.repositories.annotation.CustomLogger;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class DialogRepositoryImpl extends AbstractBaseRepository<Dialog> implements DialogRepository {
    @CustomLogger
    private Logger logger;
    private static final String PARAMETER_ID = "id";
    private static final String SELECT = "from DialogMember";
    private static final String SELECT_BY_USER_ID = SELECT + " where id_user = :" + PARAMETER_ID;
    private static final String SELECT_WITH_MESSAGE = "SELECT dialog from Dialog dialog LEFT JOIN FETCH dialog.messages where dialog.id = :" + PARAMETER_ID;

    public DialogRepositoryImpl() {
        super(Dialog.class);
    }

    @Override
    protected void setErrorMessage(String message) {
        logger.error(message);
    }

    @Override
    public List<Dialog> findByUserId(Long id) {
        logger.info("Find users dialog " + id);
        Map<String, Object> map = new HashMap<>();
        map.put(PARAMETER_ID, id);

        return super.findEntitiesWithParam(SELECT_BY_USER_ID, map);
    }

    @Override
    public Dialog getDialogWithMessage(Long id) {
        logger.info("Dialog with message " + id);
        Map<String, Object> map = new HashMap<>();
        map.put(PARAMETER_ID, id);
        Dialog dialog = super.findEntityWithParam(SELECT_WITH_MESSAGE, map);

        return dialog;
    }

    @Override
    public List<Dialog> findAll() {
        return null;
    }
}
