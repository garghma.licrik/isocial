package com.isocial.repositories.impl;

import com.isocial.models.Relationship;
import com.isocial.repositories.AbstractBaseRepository;
import com.isocial.repositories.FriendRepository;
import com.isocial.repositories.annotation.CustomLogger;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;

@Repository
public class FriendRepositoryImpl extends AbstractBaseRepository<Relationship> implements FriendRepository {
    @CustomLogger
    private Logger logger;

    private static final String SELECT_ALL = "from Relationship";
    private static final String SELECT_FROM_FRIENDS_BY_USER_AND_STATE = "FROM Relationship WHERE fromUser.username = :" + PARAMETER_NAME +
                            " AND state = :" + PARAMETER_STATE;
    private static final String SELECT_TO_FRIENDS_BY_USER_AND_STATE = "FROM Relationship WHERE toUser.username = :" + PARAMETER_NAME +
            " AND state = :" + PARAMETER_STATE;

    public FriendRepositoryImpl() {
        super(Relationship.class);
    }

    @Override
    protected void setErrorMessage(String message) {
        logger.error(message);
    }

    @Override
    public List<Relationship> findFriendsFromUserByState(String username, boolean state) {
        logger.info("Friends from user by state " + state);
        HashMap<String, Object> map = new HashMap<>();
        map.put(PARAMETER_NAME, username);
        map.put(PARAMETER_STATE, state);

        return super.findEntitiesWithParam(SELECT_FROM_FRIENDS_BY_USER_AND_STATE, map);
    }

    @Override
    public List<Relationship> findFriendsToUserByState(String username, boolean state) {
        logger.info("Friends to user by state " + state);
        HashMap<String, Object> map = new HashMap<>();
        map.put(PARAMETER_NAME, username);
        map.put(PARAMETER_STATE, state);

        return super.findEntitiesWithParam(SELECT_TO_FRIENDS_BY_USER_AND_STATE, map);
    }

    @Override
    public List<Relationship> findAll() {
        return super.findEntitiesByQuery(SELECT_ALL);
    }
}
