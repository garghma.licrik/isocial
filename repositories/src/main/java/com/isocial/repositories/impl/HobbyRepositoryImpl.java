package com.isocial.repositories.impl;

import com.isocial.models.Hobby;
import com.isocial.repositories.AbstractBaseRepository;
import com.isocial.repositories.HobbyRepository;
import com.isocial.repositories.annotation.CustomLogger;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class HobbyRepositoryImpl extends AbstractBaseRepository<Hobby> implements HobbyRepository {
    @CustomLogger
    private Logger logger;
    private static final String PARAMETER_NAME = "name";
    private static final String SELECT = "from Hobby";
    private static final String SELECT_BY_NAME = "from Hobby where name like :" + PARAMETER_NAME;

    public HobbyRepositoryImpl() {
        super(Hobby.class);
    }

    @Override
    protected void setErrorMessage(String message) {
        logger.error(message);
    }

    @Override
    public Hobby findByName(String name) {
        logger.info("Hobby " + name);
        Map<String, Object> map = new HashMap<>();
        map.put(PARAMETER_NAME, name);

        return super.findEntityWithParam(SELECT_BY_NAME, map);
    }

    @Override
    public List<Hobby> findAll() {
        logger.info("All hobby");

        return super.findEntitiesByQuery(SELECT);
    }
}