package com.isocial.repositories.impl;

import com.isocial.models.User;
import com.isocial.repositories.AbstractBaseRepository;
import com.isocial.repositories.UserRepository;
import com.isocial.repositories.annotation.CustomLogger;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class UserRepositoryImpl extends AbstractBaseRepository<User> implements UserRepository {
    @CustomLogger
    private Logger logger;
    private static final String SELECT = "from User";
    private static final String SELECT_BY_USERNAME = SELECT + " where username = :" + PARAMETER_NAME;
    private static final String SELECT_BY_CITY = SELECT + " where city like :" + PARAMETER_NAME;
    private static final String SELECT_BY_HOBBY = "select user from User user join fetch user.hobbies hobbies where hobbies.id = :" + PARAMETER_ID;
    private static final String SELECT_WITH_POSTS = "select user from User user join fetch user.posts posts where username = :" + PARAMETER_NAME;
    private static final String SELECT_WITH_HOBBY = "select user from User user join fetch user.hobbies hobbies where username = :" + PARAMETER_NAME;
    private static final String SELECT_WITH_DIALOGS = "select user from User user join fetch user.dialogMembers dialogMembers where username = :" + PARAMETER_NAME;
    private static final String SELECT_WITH_COMMUNITIES = "select user from User user join fetch user.communityMembers communityMembers where username = :" + PARAMETER_NAME;

    public UserRepositoryImpl() {
        super(User.class);
    }

    @Override
    protected void setErrorMessage(String message) {
        logger.error(message);
    }

    @Override
    public User foundByUsername(String username) {
        logger.info("User by " + username);
        Map<String, Object> map = new HashMap<>();
        map.put(PARAMETER_NAME, username);

        return super.findEntityWithParam(SELECT_BY_USERNAME, map);
    }

    @Override
    public User foundByUsernameWithPosts(String username) {
        logger.info("User " + username + " with posts.");
        Map<String, Object> map = new HashMap<>();
        map.put(PARAMETER_NAME, username);

        return super.findEntityWithParam(SELECT_WITH_POSTS, map);
    }

    @Override
    public User foundByUsernameWithHobbies(String username) {
        logger.info("User " + username + " with hobbies.");
        Map<String, Object> map = new HashMap<>();
        map.put(PARAMETER_NAME, username);

        return super.findEntityWithParam(SELECT_WITH_HOBBY, map);
    }

    @Override
    public User foundByUsernameWithDialogs(String username) {
        logger.info("User " + username + " with dialogs.");
        Map<String, Object> map = new HashMap<>();
        map.put(PARAMETER_NAME, username);

        return super.findEntityWithParam(SELECT_WITH_DIALOGS, map);
    }

    @Override
    public User foundByUsernameWithCommunities(String username) {
        logger.info("User " + username + " with communities.");
        Map<String, Object> map = new HashMap<>();
        map.put(PARAMETER_NAME, username);

        return super.findEntityWithParam(SELECT_WITH_COMMUNITIES, map);
    }

    @Override
    public List<User> foundByCity(String city) {
        logger.info("Found by " + city);
        Map<String, Object> map = new HashMap<>();
        map.put(PARAMETER_NAME, city);

        return super.findEntitiesWithParam(SELECT_BY_CITY, map);
    }

    @Override
    public List<User> foundByHobbies(Long hobbies) {
        logger.info("Found by hobby " + hobbies);
        Map<String, Object> map = new HashMap<>();
        map.put(PARAMETER_ID, hobbies);

        return super.findEntitiesWithParam(SELECT_BY_HOBBY, map);
    }

    @Override
    public List<User> findAll() {
        logger.info("All users");

        return super.findEntitiesByQuery(SELECT);
    }
}
