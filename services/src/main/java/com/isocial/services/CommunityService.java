package com.isocial.services;

import com.isocial.models.Community;
import com.isocial.models.CommunityMember;
import com.isocial.models.Post;
import com.isocial.models.User;
import com.isocial.services.utils.CommunityAction;

import java.util.List;

public interface CommunityService {
    Community findCommunityById(Long id);
    List<Community> getAllCommunities();
    Community findCommunitiesByName(String name);
    List<Community> getUserCommunities(User user);

    Community createCommunity(Community community, User currentUser);
    Community actionCommunity(User user, Long idCommunity, CommunityAction action);
    void removeCommunity(String username, Long id);
    Community createPost(Long idCommunity, String username, Post post);
    Community removePost(Long idCommunity, String username, Long id);
    Community updatePost(Long idCommunity, String username, Post post);

    List<CommunityMember> getCommunityMembers(Long id);
}