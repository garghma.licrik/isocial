package com.isocial.services;

import com.isocial.models.Dialog;
import com.isocial.models.Message;
import com.isocial.models.User;

import java.util.List;

public interface DialogService {
    List<Dialog> getAllUserDialogs(User userWithDialogs);
    Dialog getDialogById(Long id, String username);
    Dialog createDialog(User from, User to, String name);
    void removeDialog(Dialog dialog, String username);
    Dialog decodeDialog(Dialog dialog);

    Dialog createMessage(Long id, Message message, User currentUser);
    Dialog updateMessage(Long id, Message message, String username);
    Dialog removeMessage(Long id, Message message, String username);
}
