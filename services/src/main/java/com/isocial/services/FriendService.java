package com.isocial.services;

import com.isocial.models.Friend;
import com.isocial.models.User;
import com.isocial.services.utils.FriendsSection;

import java.util.List;

public interface FriendService {
    List<Friend> getActiveFriends(String username);
    List<Friend> getFriendsBySection(String username, FriendsSection section);

    void createFriendRequest(User from, User to);
    void deleteFriend(Long id);
    void acceptFriendRequest(Long id);
}
