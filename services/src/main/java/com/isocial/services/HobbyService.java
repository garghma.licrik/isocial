package com.isocial.services;

import com.isocial.models.Hobby;

import java.util.List;

public interface HobbyService {
    List<Hobby> getAllHobbies();
    void insertNewHobby(Hobby hobby);
    void removeById(Long id);
    Hobby updateHobby(Long id, Hobby hobby);

    Hobby getHobbyById(Long id);
    Hobby getHobbyByName(String name);
}
