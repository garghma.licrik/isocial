package com.isocial.services;

import com.isocial.models.User;

import java.util.List;

public interface MessageEncryption {
    void setCurrentDialogMembers(List<User> users);
    String encoding(String message);
    String decoding(String message);
}
