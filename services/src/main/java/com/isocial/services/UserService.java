package com.isocial.services;

import com.isocial.models.Post;
import com.isocial.models.User;
import com.isocial.services.utils.UserFoundOption;

import java.util.List;

public interface UserService {
    User foundByUsername(String username);
    User getWithAllInformation(String username);
    User getWithPosts(String username);
    User getWithDialogs(String username);
    User getWithCommunities(String username);
    User updateUser(User user);
    List<User> getAllUsers();
    List<User> findUsers(UserFoundOption value);
    User createPost(String username, Post post);
    User updatePost(String username, Post post);
    User removePost(String username, Post post);

    String createToken(User user);
    boolean authorization(String basePassword, String password);
    void registration(User user);
}