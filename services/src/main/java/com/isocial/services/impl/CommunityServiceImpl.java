package com.isocial.services.impl;

import com.isocial.models.Community;
import com.isocial.models.CommunityMember;
import com.isocial.models.Post;
import com.isocial.models.User;
import com.isocial.models.utils.Role;
import com.isocial.repositories.CommunityRepository;
import com.isocial.repositories.exception.RepositoryException;
import com.isocial.services.CommunityService;
import com.isocial.services.exeption.ServiceException;
import com.isocial.services.utils.CommunityAction;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.LazyInitializationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CommunityServiceImpl implements CommunityService {
    private final Logger logger;
    private final CommunityRepository communityRepository;

    public CommunityServiceImpl(CommunityRepository communityRepository) {
        this.communityRepository = communityRepository;
        this.logger = LogManager.getLogger(CommunityServiceImpl.class.getName());
    }

    @Override
    public Community findCommunityById(Long id) {
        try {
            logger.info("Find community by " + id);
            return communityRepository.getWithPostsById(id);
        } catch (RepositoryException exception) {
            if  (exception.getMessage().equals("No entity found for query")) {
                logger.debug("No entity in community by " + id);

                return communityRepository.findById(id);
            }
            logger.error("Community with id " + id + ": " + exception.getMessage());

            throw new ServiceException(exception.getMessage());
        }
    }

    @Override
    public List<Community> getAllCommunities() {
        logger.info("Get all communities");

        return communityRepository.findAll();
    }

    @Override
    public Community findCommunitiesByName(String name) {
        logger.info("Find community by name " + name);

        return communityRepository.foundByName(name);
    }

    @Override
    public List<Community> getUserCommunities(User user) {
        try {
            logger.info("Ger user community " + user.getUsername());

            return user.getCommunityMembers().stream()
                    .map(CommunityMember::getCommunity)
                    .collect(Collectors.toList());
        } catch (LazyInitializationException exception) {
            logger.error("User not have community " + user.getUsername());

            return new ArrayList<>();
        }
    }

    @Override
    @Transactional
    public Community createCommunity(Community community, User currentUser) {
        logger.info("Create community " + community.getName() + " admin is " + currentUser.getUsername());

        community.setDate(LocalDateTime.now());
        List<CommunityMember> list = new ArrayList<>();
        list.add(createMember(currentUser, community, Role.ADMIN));

        community.setMembers(list);
        communityRepository.insert(community);

        return communityRepository.foundByName(community.getName());
    }

    @Override
    @Transactional
    public Community actionCommunity(User user, Long idCommunity, CommunityAction action) {
        logger.info("User " + user.getUsername() +
                " id community " + idCommunity + " " + action);

        Community community = communityRepository.getWithMembersById(idCommunity);

        if (action == CommunityAction.INVITE && !inMember(community.getMembers(), user)) {
            community.getMembers().add(createMember(user, community, Role.USER));
        }
        if (action == CommunityAction.LEAVE && inMember(community.getMembers(), user)) {
            if (havePermission(user.getUsername(), community)) {
                logger.debug("User is admin");
                communityRepository.delete(community);

                return null;
            }
            community.getMembers().removeIf(x -> x.getUser().getUsername().equals(user.getUsername()));
        }

        return communityRepository.update(community);
    }

    @Override
    @Transactional
    public void removeCommunity(String username, Long id) {
        logger.info("Remove by id " + id +
                " user = " + username);

        if (havePermission(username, communityRepository.getWithMembersById(id))) {
            communityRepository.delete(communityRepository.findById(id));
        } else {
            logger.error("No permission");
            throw new ServiceException("No permission");
        }
    }

    @Override
    @Transactional
    public Community createPost(Long idCommunity, String username, Post post) {
        logger.info("Create post community id = " + idCommunity +
                " user = " + username);

        Community community = findCommunityById(idCommunity);
        post.setCommunity(community);
        post.setCreateTime(LocalDateTime.now());

        if (havePermission(username, communityRepository.getWithMembersById(idCommunity))) {
            community.getPosts().add(post);

            return communityRepository.update(community);
        }
        else {
            logger.error("No permission");

            throw new ServiceException("No permission");
        }
    }

    @Override
    @Transactional
    public Community removePost(Long idCommunity, String username, Long id) {
        logger.info("Remove post = " + id +
                " by community = " + idCommunity +
                "user = " + username);

        Community community = findCommunityById(idCommunity);

        if (havePermission(username, communityRepository.getWithMembersById(idCommunity))) {
            community.getPosts().removeIf(x -> x.getId().equals(id));

            return communityRepository.update(community);
        } else {
            logger.error("No permission");

            throw new ServiceException("No permission");
        }
    }

    @Override
    @Transactional
    public Community updatePost(Long idCommunity, String username, Post post) {
        logger.info("Update post = " + post.getId() +
                " community = " + idCommunity +
                " user = " + username);

        Community community = findCommunityById(idCommunity);

        if (havePermission(username, communityRepository.getWithMembersById(idCommunity))) {
            community.getPosts().stream()
                    .filter(x -> x.getId().equals(post.getId()))
                    .forEach(x -> x.setMessage(post.getMessage()));

            return communityRepository.update(community);
        } else {
            logger.error("No permission");

            throw new ServiceException("No permission");
        }
    }

    @Override
    public List<CommunityMember> getCommunityMembers(Long id) {
        logger.info("Get community members by id = " + id);
        Community community = communityRepository.getWithMembersById(id);

        return community.getMembers();
    }

    private boolean havePermission(String username, Community community) {
        return community.getMembers().stream().anyMatch(x -> x.getRole() == Role.ADMIN && x.getUser().getUsername().equals(username));
    }

    private boolean inMember(List<CommunityMember> list, User user) {
        return list.stream().anyMatch(x -> x.getUser().getUsername().equals(user.getUsername()));
    }

    private CommunityMember createMember(User user, Community community, Role role) {
        CommunityMember communityMember = new CommunityMember();
        communityMember.setUser(user);
        communityMember.setCommunity(community);
        communityMember.setRole(role);

        return communityMember;
    }
}