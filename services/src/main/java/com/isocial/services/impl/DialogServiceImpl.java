package com.isocial.services.impl;

import com.isocial.models.Dialog;
import com.isocial.models.DialogMember;
import com.isocial.models.Message;
import com.isocial.models.User;
import com.isocial.repositories.DialogRepository;
import com.isocial.services.DialogService;
import com.isocial.services.MessageEncryption;
import com.isocial.services.exeption.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.LazyInitializationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class DialogServiceImpl implements DialogService {
    private final DialogRepository dialogRepository;
    private final MessageEncryption messageEncryption;
    private final Logger logger;

    public DialogServiceImpl(DialogRepository dialogRepository, MessageEncryption messageEncryption) {
        this.dialogRepository = dialogRepository;
        this.messageEncryption = messageEncryption;
        this.logger = LogManager.getLogger(DialogServiceImpl.class.getName());
    }

    @Override
    public List<Dialog> getAllUserDialogs(User userWithDialogs) {
        try {
            logger.info("Get all dialogs");

            return userWithDialogs.getDialogMembers().stream()
                    .map(DialogMember::getDialog)
                    .collect(Collectors.toList());
        } catch (LazyInitializationException exception) {
            return new ArrayList<>();
        }
    }

    @Override
    @Transactional
    public Dialog getDialogById(Long id, String username) {
        logger.info("Get dialog by id = " + id);

        Dialog dialog = dialogRepository.getDialogWithMessage(id);

        if (havePermission(dialog, username)) {
            dialog.getMembers().stream()
                    .filter(member -> member.getUser().getUsername().equals(username))
                    .forEach(member -> setIdInMemberAndMarkMessage(member, dialog.getMessages()));

            return dialogRepository.update(dialog);
        } else {
            logger.debug("No permission");

            throw new ServiceException("No permission");
        }
    }

    @Override
    @Transactional
    public Dialog createDialog(User from, User to, String name) {
        logger.info("Create dialog");

        Dialog dialog = cratedDialogWithUsers(from, to);
        if (dialog == null) {
            dialog = createDialogWithDialogMember(from, to, name);

            dialogRepository.insert(dialog);
        }

        return dialog;
    }

    @Override
    @Transactional
    public void removeDialog(Dialog dialog, String username) {
        logger.info("Remove dialog id = " + dialog.getId());

        dialog = dialogRepository.findById(dialog.getId());

        if (havePermission(dialog, username)) {
            dialogRepository.delete(dialog);
        } else {
            logger.debug("No permission");

            throw new ServiceException("No permission");
        }
    }

    @Override
    @Transactional
    public Dialog createMessage(Long id, Message message, User currentUser) {
        logger.info("Create message");

        Dialog dialog = dialogRepository.getDialogWithMessage(id);

        if (havePermission(dialog, currentUser.getUsername())) {
            setUserInEncoder(dialog.getMembers());
            message.setMessage(messageEncryption.encoding(message.getMessage()));

            message.setDialog(dialog);
            message.setDateTime(LocalDateTime.now());
            message.setUser(currentUser);
            dialog.getMessages().add(message);

            return dialogRepository.update(dialog);
        } else {
            logger.debug("No permission");

            throw new ServiceException("No permission");
        }
    }

    @Override
    @Transactional
    public Dialog updateMessage(Long id, Message message, String username) {
        logger.info("Update message");

        Dialog dialog = dialogRepository.getDialogWithMessage(id);

        if (havePermission(dialog, username)) {
            setUserInEncoder(dialog.getMembers());

            dialog.getMessages().stream()
                    .filter(x -> x.getId().equals(message.getId()))
                    .forEach(x -> x.setMessage(messageEncryption.encoding(message.getMessage())));

            return dialogRepository.update(dialog);
        } else {
            logger.debug("No permission");

            throw new ServiceException("No permission");
        }
    }

    @Override
    @Transactional
    public Dialog removeMessage(Long id, Message message, String username) {
        logger.info("Remove message " + id);

        Dialog dialog = dialogRepository.getDialogWithMessage(id);

        if (havePermission(dialog, username)) {
            dialog.getMessages().removeIf(mes -> mes.getId().equals(message.getId()));

            return dialogRepository.update(dialog);
        } else {
            logger.debug("No permission");

            throw new ServiceException("No permission");
        }
    }

    @Override
    public Dialog decodeDialog(Dialog dialog) {
        setUserInEncoder(dialog.getMembers());

        List<Message> messages = dialog.getMessages();
        messages.forEach(x -> {
            x.setMessage(messageEncryption.decoding(x.getMessage()));
        });
        dialog.setMessages(messages);

        return dialog;
    }

    private Dialog cratedDialogWithUsers(User userFrom, User userTo) {
        try {
            List<DialogMember> firstList = userFrom.getDialogMembers();
            List<DialogMember> secondList = userTo.getDialogMembers();
            DialogMember present = firstList.stream().flatMap(
                    x -> secondList
                            .stream()
                            .filter(y -> x.getDialog().getId().equals(y.getDialog().getId()))
                            .limit(1)
            ).findFirst().orElse(new DialogMember());

            return present.getDialog();
        } catch (LazyInitializationException exception) {
            return null;
        }
    }

    private Dialog createDialogWithDialogMember(User first, User second, String name) {
        Dialog dialog = new Dialog();

        dialog.setName(name);
        dialog.setCreateTime(LocalDateTime.now());

        DialogMember fromMember = new DialogMember();
        fromMember.setDialog(dialog);
        fromMember.setUser(first);

        DialogMember toMember = new DialogMember();
        toMember.setDialog(dialog);
        toMember.setUser(second);

        List<DialogMember> members = new ArrayList<>();
        members.add(fromMember);
        members.add(toMember);
        dialog.setMembers(members);

        return dialog;
    }

    private boolean havePermission(Dialog dialog, String username) {
        return Objects.nonNull(dialog)
                && dialog.getMembers().stream().anyMatch(x -> x.getUser().getUsername().equals(username));
    }

    private void setIdInMemberAndMarkMessage(DialogMember member, List<Message> messages) {
        long position = member.getLastMessage() != null ? member.getLastMessage() : 0;

        messages.stream()
                .filter(x -> x.getId() > position)
                .forEach(message -> message.setWatched(false));
        member.setLastMessage(messages.isEmpty() ? null : messages.get(messages.size() - 1).getId());
    }

    private void setUserInEncoder(List<DialogMember> members) {
        messageEncryption.setCurrentDialogMembers(members.stream()
                .map(DialogMember::getUser)
                .collect(Collectors.toList()));
    }
}
