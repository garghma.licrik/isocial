package com.isocial.services.impl;

import com.isocial.models.Friend;
import com.isocial.models.Relationship;
import com.isocial.models.User;
import com.isocial.repositories.FriendRepository;
import com.isocial.services.FriendService;
import com.isocial.services.utils.FriendsSection;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class FriendsServiceImpl implements FriendService {
    private final Logger logger;
    private final FriendRepository friendRepository;

    public FriendsServiceImpl(FriendRepository friendRepository) {
        this.friendRepository = friendRepository;
        this.logger = LogManager.getLogger(FriendsServiceImpl.class.getName());
    }

    @Override
    public List<Friend> getActiveFriends(String username) {
        logger.info("Get active friend username = " + username);

        List<Friend> friends = new ArrayList<>();
        friends.addAll(getFriendsFromUserByState(username, true));
        friends.addAll(getFriendsToUserByState(username, true));

        return friends;
    }

    @Override
    public List<Friend> getFriendsBySection(String username, FriendsSection section) {
        logger.info("Get friends " + section + " by username = " + username);

        if (section == FriendsSection.ALL) {
            return getFriendsToUserByState(username, false);
        }
        if (section == FriendsSection.OUT) {
            return getFriendsFromUserByState(username, false);
        }

        return new ArrayList<>();
    }

    @Override
    @Transactional
    public void createFriendRequest(User from, User to) {
        logger.info("Create friend request from " + from.getUsername() + " to " + to.getUsername());

        Relationship relationship = new Relationship();
        relationship.setFromUser(from);
        relationship.setToUser(to);

        friendRepository.insert(relationship);
    }

    @Override
    @Transactional
    public void deleteFriend(Long id) {
        logger.info("Delete friend = " + id);

        Relationship friends = friendRepository.findById(id);

        friendRepository.delete(friends);
    }

    @Override
    @Transactional
    public void acceptFriendRequest(Long id) {
        logger.info("Accept friend = " + id);

        Relationship friend = friendRepository.findById(id);
        friend.setState(true);

        friendRepository.update(friend);
    }

    private List<Friend> getFriendsFromUserByState(String username, boolean state) {
        List<Relationship> friends = friendRepository.findFriendsFromUserByState(username, state);

        return friends.stream().map(x -> {
            Friend friend = new Friend(x.getToUser());
            friend.setId(x.getId());

            return friend;
        }).collect(Collectors.toList());
    }

    private List<Friend> getFriendsToUserByState(String username, boolean state) {
        List<Relationship> friends = friendRepository.findFriendsToUserByState(username, state);

        return friends.stream().map(x -> {
            Friend friend = new Friend(x.getFromUser());
            friend.setId(x.getId());

            return friend;
        }).collect(Collectors.toList());
    }
}
