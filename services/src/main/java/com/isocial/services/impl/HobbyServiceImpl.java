package com.isocial.services.impl;

import com.isocial.models.Hobby;
import com.isocial.repositories.HobbyRepository;
import com.isocial.repositories.exception.RepositoryException;
import com.isocial.services.HobbyService;
import com.isocial.services.exeption.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class HobbyServiceImpl implements HobbyService {
    private final Logger logger;
    private final HobbyRepository hobbyRepository;

    public HobbyServiceImpl(HobbyRepository hobbyRepository) {
        this.hobbyRepository = hobbyRepository;
        this.logger = LogManager.getLogger(HobbyServiceImpl.class.getName());
    }

    @Override
    public List<Hobby> getAllHobbies() {
        return hobbyRepository.findAll();
    }

    @Override
    @Transactional
    public void insertNewHobby(Hobby hobby) {
        try {
            hobbyRepository.findByName(hobby.getName());
            logger.debug("Hobby with " + hobby.getName() + " name have database");

            throw new ServiceException("Hobby with " + hobby.getName() + " name have database");
        } catch (RepositoryException exception) {
            logger.info("Create new hobby = " + hobby.getName());
            hobbyRepository.insert(hobby);
        }
    }

    @Override
    @Transactional
    public void removeById(Long id) {
        logger.info("Remove hobby = " + id);

        Hobby hobby = hobbyRepository.findById(id);
        hobbyRepository.delete(hobby);
    }

    @Override
    @Transactional
    public Hobby updateHobby(Long id, Hobby hobby) {
        logger.info("Update hobby = " + id);

        Hobby base = hobbyRepository.findById(id);
        base.setName(hobby.getName());

        return hobbyRepository.update(base);
    }

    @Override
    public Hobby getHobbyById(Long id) {
        return hobbyRepository.findById(id);
    }

    @Override
    public Hobby getHobbyByName(String name) {
        return hobbyRepository.findByName(name);
    }
}
