package com.isocial.services.impl;

import com.isocial.models.User;
import com.isocial.services.MessageEncryption;
import com.isocial.services.exeption.ServiceException;
import org.springframework.stereotype.Component;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.Key;
import java.util.Arrays;
import java.util.Base64;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class MessageEncryptionImpl implements MessageEncryption {
    private String keyword = "";
    private Cipher cipher;

    @Override
    public void setCurrentDialogMembers(List<User> users) {
        keyword = generateKeyword(users.stream()
                .sorted(Comparator.comparing(User::getId))
                .map(User::getUsername)
                .collect(Collectors.toList()));
    }

    @Override
    public String encoding(String message) {
        try {
            setCipher(Cipher.ENCRYPT_MODE);
            byte[] bytes = cipher.doFinal(message.getBytes(StandardCharsets.UTF_8));

            return Base64.getEncoder().encodeToString(bytes);
        } catch (Exception exception) {
            throw new ServiceException(exception.getMessage());
        }
    }

    @Override
    public String decoding(String message) {
        try {
            setCipher(Cipher.DECRYPT_MODE);

            byte[] cipherText = cipher.doFinal(Base64.getDecoder().decode(message));
            return new String(cipherText);
        } catch (Exception ex) {
            throw new ServiceException(ex.getMessage());
        }
    }

    private String generateKeyword(List<String> users) {
        StringBuilder stringBuilder = new StringBuilder();
        int index = 0;

        for (String user : users) {
            for (char ch : user.toCharArray()) {
                stringBuilder.append(ch).append(index++);
            }
        }

        return stringBuilder.toString();
    }

    private void setCipher(int mode) {
        try {
            byte[] decodedKey = Base64.getDecoder().decode(keyword);
            Key key = new SecretKeySpec(Arrays.copyOf(decodedKey, 16), "AES");
            this.cipher = Cipher.getInstance("AES");
            this.cipher.init(mode, key);
        } catch (Exception ex) {
            throw new ServiceException(ex.getMessage());
        }
    }
}