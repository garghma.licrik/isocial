package com.isocial.services.impl;

import com.isocial.models.Post;
import com.isocial.models.User;
import com.isocial.repositories.UserRepository;
import com.isocial.repositories.exception.RepositoryException;
import com.isocial.services.UserService;
import com.isocial.services.exeption.ServiceException;
import com.isocial.services.utils.JwtProvider;
import com.isocial.services.utils.UserFoundOption;
import com.isocial.services.utils.UsersData;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
public class UserServiceImpl implements UserService {
    private final Logger logger;
    private final UserRepository repository;
    private final JwtProvider provider;
    private final PasswordEncoder encoder;

    @Autowired
    public UserServiceImpl(UserRepository repository,
                           JwtProvider provider,
                           PasswordEncoder encoder) {
        this.repository = repository;
        this.provider = provider;
        this.encoder = encoder;
        this.logger = LogManager.getLogger(UserServiceImpl.class.getName());
    }

    @Override
    public User foundByUsername(String username) {
        try {
            logger.info("Found by username = " + username);

            return repository.foundByUsername(username);
        } catch (RepositoryException exception) {
            if (exception.getMessage().equals("No entity found for query")) {
                logger.debug("No entity found for query where username = " + username);

                return null;
            }
            logger.error("Username = " + username + " throw " + exception.getMessage());

            throw new ServiceException(exception.getMessage());
        }
    }

    @Override
    public User getWithAllInformation(String username) {
        logger.info("Get with all information " + username);

        return returnUserWithData(username, UsersData.HOBBIES);
    }

    @Override
    public User getWithPosts(String username) {
        logger.info("Get with posts " + username);

        return returnUserWithData(username, UsersData.POSTS);
    }

    @Override
    public User getWithDialogs(String username) {
        logger.info("Get with dialogs " + username);

        return returnUserWithData(username, UsersData.DIALOGS);
    }

    @Override
    public User getWithCommunities(String username) {
        logger.info("Get with communities " + username);

        return returnUserWithData(username, UsersData.COMMUNITIES);
    }

    @Override
    @Transactional
    public User updateUser(User user) {
        logger.info("Update user = " + user.getUsername());

        return repository.update(user);
    }

    @Override
    public List<User> getAllUsers() {
        return repository.findAll();
    }

    @Override
    public List<User> findUsers(UserFoundOption value) {
        logger.info("Find user by options");

        List<User> users = new ArrayList<>();
        if (!Objects.isNull(value.getName()) && !value.getName().isEmpty()) {
            users.add(repository.foundByUsername(value.getName()));
        }
        if (!Objects.isNull(value.getCity()) && !value.getCity().isEmpty()) {
            users.addAll(repository.foundByCity(value.getCity()));
        }
        if (!Objects.isNull(value.getHobbies_id()) && !value.getHobbies_id().isEmpty()) {
            value.getHobbies_id().forEach(x -> {
                users.addAll(repository.foundByHobbies(x));
            });
        }

        return users;
    }

    @Override
    @Transactional
    public User createPost(String username, Post post) {
        logger.info("Create post username = " + username);

        User user = getWithPosts(username);

        post.setCreateTime(LocalDateTime.now());
        post.setUser(user);
        user.getPosts().add(post);

        return repository.update(user);
    }

    @Override
    @Transactional
    public User updatePost(String username, Post post) {
        logger.info("Update posts username = " + username);

        User user = getWithPosts(username);
        user.getPosts().stream().filter(x -> x.getId().equals(post.getId())).forEach(x -> x.setMessage(post.getMessage()));

        return repository.update(user);
    }

    @Override
    @Transactional
    public User removePost(String username, Post post) {
        logger.info("Remove posts username = " + username);

        User user = getWithPosts(username);
        user.getPosts().removeIf(x -> x.getId().equals(post.getId()));

        return repository.update(user);
    }

    @Override
    public String createToken(User user) {
        return provider.createToken(user.getUsername(), user.getRole().toString());
    }

    @Override
    public boolean authorization(String basePassword, String password) {
        logger.info("Authorisation");
        return encoder.matches(password, basePassword);
    }

    @Override
    @Transactional
    public void registration(User user) {
        logger.info("Registration");

        user.setPassword(encoder.encode(user.getPassword()));

        repository.insert(user);
    }

    private User returnUserWithData(String username, UsersData data) {
        try {
            return data.getUserWithData(repository, username);
        } catch (RepositoryException exception) {
            if (exception.getMessage().equals("No entity found for query")) {
                logger.debug(exception.getMessage());
                return repository.foundByUsername(username);
            }
            logger.error(exception.getMessage());

            throw new ServiceException(exception.getMessage());
        }
    }
}