package com.isocial.services.utils;

public enum CommunityAction {
    INVITE,
    LEAVE;

    public static CommunityAction get(String action) {
        if (action.equals("invite")) {
            return CommunityAction.INVITE;
        }
        if (action.equals("leave")) {
            return CommunityAction.LEAVE;
        }

        return null;
    }
}
