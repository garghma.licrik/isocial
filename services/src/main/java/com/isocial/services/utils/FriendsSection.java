package com.isocial.services.utils;

public enum FriendsSection {
    ALL,
    OUT;

    public static FriendsSection get(String value) {
        if (value.equals("ALL")) {
            return FriendsSection.ALL;
        } else {
            return FriendsSection.OUT;
        }
    }
}
