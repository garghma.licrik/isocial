package com.isocial.services.utils;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserFoundOption {
    private String name;
    private List<Long> hobbies_id;
    private String city;
    private Character gender;
}
