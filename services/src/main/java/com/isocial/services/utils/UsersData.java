package com.isocial.services.utils;

import com.isocial.models.User;
import com.isocial.repositories.UserRepository;

public enum UsersData {
    POSTS {
        @Override
        public User getUserWithData(UserRepository repository, String username) {
            return repository.foundByUsernameWithPosts(username);
        }
    },
    DIALOGS {
        @Override
        public User getUserWithData(UserRepository repository, String username) {
            return repository.foundByUsernameWithDialogs(username);
        }
    },
    COMMUNITIES {
        @Override
        public User getUserWithData(UserRepository repository, String username) {
            return repository.foundByUsernameWithCommunities(username);
        }
    },
    HOBBIES {
        @Override
        public User getUserWithData(UserRepository repository, String username) {
            return repository.foundByUsernameWithHobbies(username);
        }
    };

    public abstract User getUserWithData(UserRepository repository, String username);
}
