package com.isocial.services;

import com.isocial.models.Community;
import com.isocial.models.CommunityMember;
import com.isocial.models.Post;
import com.isocial.models.User;
import com.isocial.models.utils.Role;
import com.isocial.repositories.CommunityRepository;
import com.isocial.repositories.exception.RepositoryException;
import com.isocial.services.exeption.ServiceException;
import com.isocial.services.impl.CommunityServiceImpl;
import com.isocial.services.utils.*;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;


public class TestCommunityService {
    private static final String EXCEPTION_MESSAGE = "No entity found for query";

    private final CommunityService communityService;
    private final CommunityRepository communityRepository;
    private final CommunityFactory communityFactory = new CommunityFactory();
    private final PostFactory postFactory = new PostFactory();
    private final UserFactory userFactory = new UserFactory();
    private final CommunityMemberFactory communityMemberFactory = new CommunityMemberFactory();

    private int count = 10;

    public TestCommunityService() {
        communityRepository = Mockito.mock(CommunityRepository.class);
        communityService = new CommunityServiceImpl(communityRepository);
    }

    @Test
    public void test_foundById_WithPosts_WithoutException() {
        Community testCommunity = communityFactory.createEntity();
        testCommunity.setPosts(postFactory.createList(count));
        Mockito.when(communityRepository.getWithPostsById(Mockito.anyLong()))
                .thenReturn(testCommunity);

        Community resultCommunity = communityService.findCommunityById(Mockito.anyLong());

        Mockito.verify(communityRepository, Mockito.times(1)).getWithPostsById(Mockito.anyLong());
        Assertions.assertEquals(testCommunity, resultCommunity);
    }

    @Test
    public void test_foundById_WithoutPosts_WithoutException() {
        Community testCommunity = communityFactory.createEntity();
        Mockito.when(communityRepository.getWithPostsById(Mockito.anyLong()))
                .thenThrow(new RepositoryException(EXCEPTION_MESSAGE));
        Mockito.when(communityRepository.findById(Mockito.anyLong()))
                .thenReturn(testCommunity);

        Community resultCommunity = communityService.findCommunityById(Mockito.anyLong());

        Mockito.verify(communityRepository, Mockito.times(1)).getWithPostsById(Mockito.anyLong());
        Assertions.assertEquals(testCommunity, resultCommunity);
    }

    @Test
    public void test_foundById_WithoutException() {
        Mockito.when(communityRepository.getWithPostsById(Mockito.anyLong()))
                .thenThrow(new RepositoryException(StaticConstant.BAD_EXCEPTION_MESSAGE));
        try {
            Community resultCommunity = communityService.findCommunityById(Mockito.anyLong());
        } catch (ServiceException exception) {
            Mockito.verify(communityRepository, Mockito.times(1))
                    .getWithPostsById(Mockito.anyLong());
            Mockito.verify(communityRepository, Mockito.never())
                    .findById(Mockito.anyLong());

            Assertions.assertEquals(exception.getMessage(), StaticConstant.BAD_EXCEPTION_MESSAGE);
        }
    }

    @Test
    public void test_getUserCommunity_WithoutException() {
        User testUser = userFactory.createEntity();
        List<CommunityMember> communityMembers = communityMemberFactory.createList(count);
        Community community = communityFactory.createEntity();

        communityMembers.forEach(x -> x.setCommunity(community));
        testUser.setCommunityMembers(communityMembers);

        List<Community> resultCommunities = communityService.getUserCommunities(testUser);

        resultCommunities.forEach(x -> Assertions.assertEquals(x, community));
    }

    @Test
    public void test_createCommunity() {
        Community testCommunity = communityFactory.createEntity();
        User testUser = userFactory.createEntity();
        testUser.setUsername("test");

        Mockito.when(communityRepository.foundByName(Mockito.anyString())).thenReturn(testCommunity);

        Community resultCommunity = communityService.createCommunity(testCommunity, testUser);

        Assertions.assertNotEquals(resultCommunity, testCommunity);
    }

    @Test
    public void test_removeCommunity() {
        User testUser = userFactory.createEntity();
        CommunityMember testCommunityMember = communityMemberFactory.createEntity();
        Community testCommunity = communityFactory.createEntity();

        testUser.setUsername(StaticConstant.TEST_STRING);
        testCommunityMember.setUser(testUser);
        testCommunityMember.setRole(Role.ADMIN);
        testCommunity.setMembers(new ArrayList<>());
        testCommunity.getMembers().add(testCommunityMember);
        Mockito.when(communityRepository.getWithMembersById(Mockito.anyLong()))
                .thenReturn(testCommunity);
        Mockito.when(communityRepository.findById(Mockito.anyLong()))
                .thenReturn(testCommunity);

        communityService.removeCommunity(testUser.getUsername(), Mockito.anyLong());

        Mockito.verify(communityRepository, Mockito.times(1))
                .findById(Mockito.anyLong());
        Mockito.verify(communityRepository, Mockito.times(1))
                .getWithMembersById(Mockito.anyLong());
        Mockito.verify(communityRepository, Mockito.times(1))
                .delete(testCommunity);
    }

    @Test
    public void test_removeCommunity_noPermission() {
        User testUser = createUserWithUsername();
        CommunityMember testCommunityMember = createTestMemberWithoutPermission(testUser);
        Community testCommunity = communityFactory.createEntity();

        testCommunity.setMembers(new ArrayList<>());
        testCommunity.getMembers().add(testCommunityMember);
        Mockito.when(communityRepository.getWithMembersById(Mockito.anyLong()))
                .thenReturn(testCommunity);
        Mockito.when(communityRepository.findById(Mockito.anyLong()))
                .thenReturn(testCommunity);

        try {
            communityService.removeCommunity(testUser.getUsername(), Mockito.anyLong());
        } catch (ServiceException exception) {
            Assertions.assertEquals(exception.getMessage(), "No permission");
        }

        Mockito.verify(communityRepository, Mockito.never())
                .findById(Mockito.anyLong());
        Mockito.verify(communityRepository, Mockito.times(1))
                .getWithMembersById(Mockito.anyLong());
        Mockito.verify(communityRepository, Mockito.never())
                .delete(testCommunity);
    }

    @Test
    public void test_createPost_withPermission() {
        User testUser = createUserWithUsername();
        CommunityMember testCommunityMember = createTestMemberWithPermission(testUser);
        Community testCommunity = communityFactory.createEntity();
        Post testPost = postFactory.createEntity();

        testCommunity.setPosts(postFactory.createList(count));
        testCommunity.setMembers(new ArrayList<>());
        testCommunity.getMembers().add(testCommunityMember);
        Mockito.when(communityRepository.getWithPostsById(Mockito.anyLong()))
                .thenReturn(testCommunity);
        Mockito.when(communityRepository.getWithMembersById(Mockito.anyLong()))
                .thenReturn(testCommunity);

        Community resultCommunity = communityService.createPost(Mockito.anyLong(), testUser.getUsername(), testPost);

        Assertions.assertNotEquals(testCommunity, resultCommunity);
        Mockito.verify(communityRepository, Mockito.times(1)).update(Mockito.any());
        Mockito.verify(communityRepository, Mockito.times(1)).getWithPostsById(Mockito.anyLong());
        Mockito.verify(communityRepository, Mockito.times(1)).getWithMembersById(Mockito.anyLong());
    }

    @Test
    public void test_createPost_withoutPermission() {
        User testUser = createUserWithUsername();
        CommunityMember testCommunityMember = createTestMemberWithoutPermission(testUser);
        Community testCommunity = communityFactory.createEntity();
        Post testPost = postFactory.createEntity();
        String messageError = "";

        testCommunity.setPosts(postFactory.createList(count));
        testCommunity.setMembers(new ArrayList<>());
        testCommunity.getMembers().add(testCommunityMember);
        Mockito.when(communityRepository.getWithPostsById(Mockito.anyLong()))
                .thenReturn(testCommunity);
        Mockito.when(communityRepository.getWithMembersById(Mockito.anyLong()))
                .thenReturn(testCommunity);

        try {
            Community resultCommunity = communityService.createPost(Mockito.anyLong(), testUser.getUsername(), testPost);
        } catch (ServiceException exception) {
            messageError = exception.getMessage();
        }

        Assertions.assertEquals(messageError, "No permission");
        Mockito.verify(communityRepository, Mockito.never()).update(Mockito.any());
        Mockito.verify(communityRepository, Mockito.times(1)).getWithPostsById(Mockito.anyLong());
        Mockito.verify(communityRepository, Mockito.times(1)).getWithMembersById(Mockito.anyLong());
    }

    @Test
    public void test_removePost_withPermission() {
        User testUser = createUserWithUsername();
        CommunityMember testCommunityMember = createTestMemberWithPermission(testUser);
        Community testCommunity = communityFactory.createEntity();

        testCommunity.setPosts(postFactory.createList(count));
        testCommunity.setMembers(new ArrayList<>());
        testCommunity.getMembers().add(testCommunityMember);
        Mockito.when(communityRepository.getWithPostsById(Mockito.anyLong()))
                .thenReturn(testCommunity);
        Mockito.when(communityRepository.getWithMembersById(Mockito.anyLong()))
                .thenReturn(testCommunity);
        Mockito.when(communityRepository.update(Mockito.any(Community.class)))
                .thenAnswer(i -> i.getArguments()[0]);
        int first = testCommunity.getPosts().size();

        Community resultCommunity = communityService.removePost(Mockito.anyLong(), testUser.getUsername(), Integer.toUnsignedLong(0));


        int second = resultCommunity.getPosts().size();
        Assertions.assertNotEquals(first, second);
        Mockito.verify(communityRepository, Mockito.times(1)).update(Mockito.any());
        Mockito.verify(communityRepository, Mockito.times(1)).getWithPostsById(Mockito.anyLong());
        Mockito.verify(communityRepository, Mockito.times(1)).getWithMembersById(Mockito.anyLong());
    }

    @Test
    public void test_removePost_withPermission_noIdInList() {
        User testUser = createUserWithUsername();
        CommunityMember testCommunityMember = createTestMemberWithPermission(testUser);
        Community testCommunity = communityFactory.createEntity();

        testCommunity.setPosts(postFactory.createList(count));
        testCommunity.setMembers(new ArrayList<>());
        testCommunity.getMembers().add(testCommunityMember);
        Mockito.when(communityRepository.getWithPostsById(Mockito.anyLong()))
                .thenReturn(testCommunity);
        Mockito.when(communityRepository.getWithMembersById(Mockito.anyLong()))
                .thenReturn(testCommunity);
        Mockito.when(communityRepository.update(Mockito.any(Community.class)))
                .thenAnswer(i -> i.getArguments()[0]);
        int first = testCommunity.getPosts().size();

        Community resultCommunity = communityService.removePost(Mockito.anyLong(), testUser.getUsername(), (long) 1000);


        int second = resultCommunity.getPosts().size();
        Assertions.assertEquals(first, second);
        Mockito.verify(communityRepository, Mockito.times(1)).update(Mockito.any());
        Mockito.verify(communityRepository, Mockito.times(1)).getWithPostsById(Mockito.anyLong());
        Mockito.verify(communityRepository, Mockito.times(1)).getWithMembersById(Mockito.anyLong());
    }

    @Test
    public void test_removePost_withoutPermission() {
        User testUser = createUserWithUsername();
        CommunityMember testCommunityMember = createTestMemberWithoutPermission(testUser);
        Community testCommunity = communityFactory.createEntity();
        String messageException = "";

        testCommunity.setPosts(postFactory.createList(count));
        testCommunity.setMembers(new ArrayList<>());
        testCommunity.getMembers().add(testCommunityMember);
        Mockito.when(communityRepository.getWithPostsById(Mockito.anyLong()))
                .thenReturn(testCommunity);
        Mockito.when(communityRepository.getWithMembersById(Mockito.anyLong()))
                .thenReturn(testCommunity);

        try {
            Community resultCommunity = communityService.removePost(Mockito.anyLong(), testUser.getUsername(), Integer.toUnsignedLong(0));
        } catch (ServiceException exception) {
            messageException = exception.getMessage();
        }

        Assertions.assertEquals(messageException, "No permission");
        Mockito.verify(communityRepository, Mockito.never()).update(Mockito.any());
        Mockito.verify(communityRepository, Mockito.times(1))
                .getWithPostsById(Mockito.anyLong());
        Mockito.verify(communityRepository, Mockito.times(1))
                .getWithMembersById(Mockito.anyLong());
    }

    @Test
    public void test_updatePost_withPermission() {
        long id = 5;
        User testUser = createUserWithUsername();
        CommunityMember testCommunityMember = createTestMemberWithPermission(testUser);
        Community testCommunity = communityFactory.createEntity();
        Post testPost = postFactory.createEntity();

        testPost.setId(id);
        testPost.setMessage("New message");
        testCommunity.setPosts(postFactory.createList(count));
        testCommunity.setMembers(new ArrayList<>());
        testCommunity.getMembers().add(testCommunityMember);
        Mockito.when(communityRepository.getWithPostsById(Mockito.anyLong()))
                .thenReturn(testCommunity);
        Mockito.when(communityRepository.getWithMembersById(Mockito.anyLong()))
                .thenReturn(testCommunity);
        Mockito.when(communityRepository.update(Mockito.any(Community.class)))
                .thenAnswer(i -> i.getArguments()[0]);

        Community resultCommunity = communityService.updatePost(Mockito.anyLong(), testUser.getUsername(), testPost);

        Assertions.assertEquals(resultCommunity.getPosts().get((int) id), testPost);
        Mockito.verify(communityRepository, Mockito.times(1)).update(Mockito.any());
        Mockito.verify(communityRepository, Mockito.times(1)).getWithPostsById(Mockito.anyLong());
        Mockito.verify(communityRepository, Mockito.times(1)).getWithMembersById(Mockito.anyLong());
    }

    @Test
    public void test_updatePost_withPermission_notInList() {
        long id = 1000;
        User testUser = createUserWithUsername();
        CommunityMember testCommunityMember = createTestMemberWithPermission(testUser);
        Community testCommunity = communityFactory.createEntity();
        Post testPost = postFactory.createEntity();

        testPost.setId(id);
        testPost.setMessage("New message");
        testCommunity.setPosts(postFactory.createList(count));
        testCommunity.setMembers(new ArrayList<>());
        testCommunity.getMembers().add(testCommunityMember);
        Mockito.when(communityRepository.getWithPostsById(Mockito.anyLong()))
                .thenReturn(testCommunity);
        Mockito.when(communityRepository.getWithMembersById(Mockito.anyLong()))
                .thenReturn(testCommunity);
        Mockito.when(communityRepository.update(Mockito.any(Community.class)))
                .thenAnswer(i -> i.getArguments()[0]);

        Community resultCommunity = communityService.updatePost(Mockito.anyLong(), testUser.getUsername(), testPost);

        resultCommunity.getPosts().forEach(x -> Assertions.assertNotEquals(x, testPost));
        Mockito.verify(communityRepository, Mockito.times(1)).update(Mockito.any());
        Mockito.verify(communityRepository, Mockito.times(1)).getWithPostsById(Mockito.anyLong());
        Mockito.verify(communityRepository, Mockito.times(1)).getWithMembersById(Mockito.anyLong());
    }

    @Test
    public void test_communityActionInvite_userNotMember() {
        User testUser = createUserWithUsername();
        Community testCommunity = communityFactory.createEntity();

        testCommunity.setMembers(new ArrayList<>());
        Mockito.when(communityRepository.getWithMembersById(Mockito.anyLong()))
                .thenReturn(testCommunity);
        Mockito.when(communityRepository.update(Mockito.any(Community.class)))
                .thenAnswer(i -> i.getArguments()[0]);

        Community resultCommunity = communityService.actionCommunity(testUser, Mockito.anyLong(), CommunityAction.INVITE);

        Assertions.assertEquals(resultCommunity.getMembers().size(), 1);
        Mockito.verify(communityRepository, Mockito.times(1)).update(Mockito.any());
        Mockito.verify(communityRepository, Mockito.times(1)).getWithMembersById(Mockito.anyLong());
    }

    @Test
    public void test_communityActionInvite_userIsMember() {
        User testUser = createUserWithUsername();
        CommunityMember testCommunityMember = createTestMemberWithoutPermission(testUser);
        Community testCommunity = communityFactory.createEntity();

        testCommunity.setMembers(new ArrayList<>());
        testCommunity.getMembers().add(testCommunityMember);
        Mockito.when(communityRepository.getWithMembersById(Mockito.anyLong()))
                .thenReturn(testCommunity);
        Mockito.when(communityRepository.update(Mockito.any(Community.class)))
                .thenAnswer(i -> i.getArguments()[0]);

        Community resultCommunity = communityService.actionCommunity(testUser, Mockito.anyLong(), CommunityAction.INVITE);

        Assertions.assertEquals(resultCommunity.getMembers().size(), 1);
        Mockito.verify(communityRepository, Mockito.times(1))
                .update(Mockito.any());
        Mockito.verify(communityRepository, Mockito.times(1))
                .getWithMembersById(Mockito.anyLong());
    }

    @Test
    public void test_communityActionLeave_userInMember() {
        User testUser = createUserWithUsername();
        CommunityMember testCommunityMember = createTestMemberWithoutPermission(testUser);
        Community testCommunity = communityFactory.createEntity();

        testCommunity.setMembers(new ArrayList<>());
        testCommunity.getMembers().add(testCommunityMember);
        Mockito.when(communityRepository.getWithMembersById(Mockito.anyLong()))
                .thenReturn(testCommunity);
        Mockito.when(communityRepository.update(Mockito.any(Community.class)))
                .thenAnswer(i -> i.getArguments()[0]);

        Community resultCommunity = communityService.actionCommunity(testUser, Mockito.anyLong(), CommunityAction.LEAVE);

        Assertions.assertEquals(resultCommunity.getMembers().size(), 0);
        Mockito.verify(communityRepository, Mockito.times(1))
                .update(Mockito.any());
        Mockito.verify(communityRepository, Mockito.times(1))
                .getWithMembersById(Mockito.anyLong());
    }

    @Test
    public void test_communityActionLeave_userOutMember() {
        User testUser = createUserWithUsername();
        Community testCommunity = communityFactory.createEntity();

        testCommunity.setMembers(new ArrayList<>());
        Mockito.when(communityRepository.getWithMembersById(Mockito.anyLong()))
                .thenReturn(testCommunity);
        Mockito.when(communityRepository.update(Mockito.any(Community.class)))
                .thenAnswer(i -> i.getArguments()[0]);

        Community resultCommunity = communityService.actionCommunity(testUser, Mockito.anyLong(), CommunityAction.LEAVE);

        Assertions.assertEquals(resultCommunity.getMembers().size(), 0);
        Mockito.verify(communityRepository, Mockito.times(1))
                .update(Mockito.any());
        Mockito.verify(communityRepository, Mockito.times(1))
                .getWithMembersById(Mockito.anyLong());
    }

    @Test
    public void test_communityActionLeave_userHavePermission() {
        User testUser = createUserWithUsername();
        CommunityMember testCommunityMember = createTestMemberWithPermission(testUser);
        Community testCommunity = communityFactory.createEntity();

        testCommunity.setMembers(new ArrayList<>());
        testCommunity.getMembers().add(testCommunityMember);
        Mockito.when(communityRepository.getWithMembersById(Mockito.anyLong()))
                .thenReturn(testCommunity);
        Mockito.when(communityRepository.update(Mockito.any(Community.class)))
                .thenAnswer(i -> i.getArguments()[0]);

        Community resultCommunity = communityService.actionCommunity(testUser, Mockito.anyLong(), CommunityAction.LEAVE);

        Assertions.assertNull(resultCommunity);
        Mockito.verify(communityRepository, Mockito.never())
                .update(Mockito.any());
        Mockito.verify(communityRepository, Mockito.times(1))
                .getWithMembersById(Mockito.anyLong());
        Mockito.verify(communityRepository, Mockito.times(1))
                .delete(Mockito.any());
    }

    private User createUserWithUsername() {
        User testUser = userFactory.createEntity();
        testUser.setUsername(StaticConstant.TEST_STRING);

        return testUser;
    }

    private CommunityMember createTestMemberWithPermission(User user) {
        CommunityMember testCommunityMember = communityMemberFactory.createEntity();

        testCommunityMember.setUser(user);
        testCommunityMember.setRole(Role.ADMIN);

        return testCommunityMember;
    }

    private CommunityMember createTestMemberWithoutPermission(User user) {
        CommunityMember testCommunityMember = communityMemberFactory.createEntity();

        testCommunityMember.setUser(user);
        testCommunityMember.setRole(Role.USER);

        return testCommunityMember;
    }
}
