package com.isocial.services;

import com.isocial.models.Dialog;
import com.isocial.models.DialogMember;
import com.isocial.models.Message;
import com.isocial.models.User;
import com.isocial.repositories.DialogRepository;
import com.isocial.services.impl.DialogServiceImpl;
import com.isocial.services.utils.DialogFactory;
import com.isocial.services.utils.DialogMemberFactory;
import com.isocial.services.utils.MessageFactory;
import com.isocial.services.utils.UserFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.mockito.Mockito;

import java.util.ArrayList;

public class TestDialogService {
    private static final String USERNAME = "test";

    private final DialogRepository dialogRepository;
    private final MessageEncryption messageEncryption;
    private final DialogServiceImpl dialogService;
    private final DialogMemberFactory dialogMemberFactory;
    private final DialogFactory dialogFactory;
    private final MessageFactory messageFactory;
    private final UserFactory userFactory;

    private Dialog testDialog;
    private User testUser;
    private int count = 10;

    public TestDialogService() {
        this.messageEncryption = Mockito.mock(MessageEncryption.class);
        this.dialogRepository = Mockito.mock(DialogRepository.class);
        dialogService = new DialogServiceImpl(this.dialogRepository, this.messageEncryption);
        this.dialogMemberFactory = new DialogMemberFactory();
        this.dialogFactory = new DialogFactory();
        this.messageFactory = new MessageFactory();
        this.userFactory = new UserFactory();
    }

    @Test
    public void test_getDialogById() {
        Dialog resultDialog = dialogService.getDialogById(Mockito.anyLong(), testUser.getUsername());

        Assertions.assertEquals(resultDialog, testDialog);
    }

    @Test
    public void test_createDialog() {
        User toUser = userFactory.createEntity();
        User fromUser = userFactory.createEntity();
        String name  = "test";
        toUser.setDialogMembers(new ArrayList<>());
        fromUser.setDialogMembers(new ArrayList<>());

        Dialog dialog = dialogService.createDialog(fromUser, toUser, name);

        Assertions.assertNotNull(dialog);
        Mockito.verify(dialogRepository, Mockito.times(1))
                .insert(Mockito.any(Dialog.class));
    }

    @Test
    public void test_removeDialog() {
        dialogService.removeDialog(this.testDialog, this.testUser.getUsername());

        Mockito.verify(dialogRepository, Mockito.times(1))
                .findById(Mockito.anyLong());
        Mockito.verify(dialogRepository, Mockito.times(1))
                .delete(this.testDialog);
    }

    @Test
    public void test_createMessage() {
        Dialog dialog = dialogService.createMessage(Mockito.anyLong(),
                messageFactory.createEntity(),
                testUser);

        Assertions.assertNotEquals(count, dialog.getMessages().size());
        Mockito.verify(messageEncryption, Mockito.times(1))
                .setCurrentDialogMembers(Mockito.anyList());
        Mockito.verify(messageEncryption, Mockito.times(1))
                .encoding(Mockito.any());
        Mockito.verify(dialogRepository, Mockito.times(1))
                .getDialogWithMessage(Mockito.anyLong());
        Mockito.verify(dialogRepository, Mockito.times(1))
                .update(dialog);
    }

    @Test
    public void test_removeMessage() {
        Message testMessage = messageFactory.createEntity();
        testMessage.setId((long) 0);

        Dialog dialog = dialogService.removeMessage(Mockito.anyLong(), testMessage, USERNAME);

        Assertions.assertNotEquals(count, dialog.getMessages().size());
        Mockito.verify(dialogRepository, Mockito.times(1))
                .getDialogWithMessage(Mockito.anyLong());
        Mockito.verify(dialogRepository, Mockito.times(1))
                .update(Mockito.any(Dialog.class));
    }

    @Test
    public void test_removeMessage_idNotInList() {
        Message testMessage = messageFactory.createEntity();
        testMessage.setId((long) 100);

        Dialog dialog = dialogService.removeMessage(Mockito.anyLong(), testMessage, USERNAME);

        Assertions.assertEquals(count, dialog.getMessages().size());
        Mockito.verify(dialogRepository, Mockito.times(1))
                .getDialogWithMessage(Mockito.anyLong());
        Mockito.verify(dialogRepository, Mockito.times(1))
                .update(Mockito.any(Dialog.class));
    }

    @Test
    public void test_updateMessage() {
        int position = 5;
        Message testMessage = messageFactory.createEntity();
        testMessage.setId((long) position);

        Dialog dialog = dialogService.updateMessage(Mockito.anyLong(), testMessage, USERNAME);

        Assertions.assertEquals(testMessage, dialog.getMessages().get(position));
        Mockito.verify(dialogRepository, Mockito.times(1))
                .getDialogWithMessage(Mockito.anyLong());
        Mockito.verify(dialogRepository, Mockito.times(1))
                .update(Mockito.any(Dialog.class));
    }

    @Before
    public void createTestDialog() {
        this.testDialog = dialogFactory.createEntity();
        this.testDialog.setMembers(dialogMemberFactory.createList(count));
        this.testDialog.setMessages(messageFactory.createList(count));
        this.testDialog.setId((long) 0);

        int index = 0;
        for (DialogMember member : this.testDialog.getMembers()) {
            member.setUser(userFactory.createEntity());
            member.getUser().setId((long) index++);
            member.getUser().setUsername(USERNAME);
        }

        this.testUser = userFactory.createEntity();
        this.testUser.setId((long) (index / 2));
        this.testUser.setUsername(USERNAME);

        baseMockAction();
    }

    private void baseMockAction() {
        Mockito.when(dialogRepository.getDialogWithMessage(Mockito.anyLong()))
                .thenReturn(this.testDialog);
        Mockito.when(dialogRepository.findByUserId(Mockito.anyLong()))
                .thenReturn(dialogFactory.createList(count));
        Mockito.when(dialogRepository.findById(Mockito.anyLong()))
                .thenReturn(this.testDialog);
        Mockito.when(dialogRepository.update(Mockito.any(Dialog.class)))
                .thenAnswer(i -> i.getArguments()[0]);

        Mockito.when(messageEncryption.decoding(Mockito.anyString()))
                .thenAnswer(i -> i.getArguments()[0]);
        Mockito.when(messageEncryption.encoding(Mockito.anyString()))
                .thenAnswer(i -> i.getArguments()[0]);
    }
}
