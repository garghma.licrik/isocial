package com.isocial.services;

import com.isocial.models.Friend;
import com.isocial.models.Relationship;
import com.isocial.models.User;
import com.isocial.repositories.FriendRepository;
import com.isocial.services.impl.FriendsServiceImpl;
import com.isocial.services.utils.FriendsSection;
import com.isocial.services.utils.RelationshipFactory;
import com.isocial.services.utils.UserFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.mockito.Mockito;

import java.util.List;

public class TestFriendService {
    private final FriendService service;
    private final FriendRepository repository;
    private final RelationshipFactory friendFactory;
    private final UserFactory userFactory;

    private int count = 10;
    private String username = "test";

    public TestFriendService() {
        this.repository = Mockito.mock(FriendRepository.class);
        this.service = new FriendsServiceImpl(this.repository);
        this.friendFactory = new RelationshipFactory();
        this.userFactory = new UserFactory();
    }

    @Test
    public void test_getActiveFriend() {
        List<Friend> friends = service.getActiveFriends(username);

        Assertions.assertEquals(friends.size(), (count + count));
        Mockito.verify(repository, Mockito.times(1))
                .findFriendsFromUserByState(username, true);
        Mockito.verify(repository, Mockito.times(1))
                .findFriendsToUserByState(username, true);
    }

    @Test
    public void test_createFriendRequest() {
        User from = userFactory.createEntity();
        User to = userFactory.createEntity();

        service.createFriendRequest(from, to);

        Mockito.verify(repository, Mockito.times(1))
                .insert(Mockito.any(Relationship.class));
    }

    @Test
    public void test_acceptFriend() {
        service.acceptFriendRequest(Mockito.anyLong());

        Mockito.verify(repository, Mockito.times(1))
                 .findById(Mockito.anyLong());
        Mockito.verify(repository, Mockito.times(1))
                 .update(Mockito.any(Relationship.class));
    }

    @Test
    public void test_getFriendsBySection_allRequest() {
        List<Friend> friends = service.getFriendsBySection(username, FriendsSection.ALL);

        Assertions.assertEquals(friends.size(), count);
        Mockito.verify(repository, Mockito.times(1))
                .findFriendsToUserByState(username, false);
    }

    @Test
    public void test_getFriendsBySection_outRequest() {
        List<Friend> friends = service.getFriendsBySection(username, FriendsSection.OUT);

        Assertions.assertEquals(friends.size(), count);
        Mockito.verify(repository, Mockito.times(1))
                .findFriendsFromUserByState(username, false);
    }

    @Test
    public void test_deleteFriends() {
        service.deleteFriend((long) 0);

        Mockito.verify(repository, Mockito.times(1))
                .findById(Mockito.anyLong());
        Mockito.verify(repository, Mockito.times(1))
                .delete(Mockito.any(Relationship.class));
    }

    @Before
    public void createBaseAction() {
        Mockito.when(repository.findFriendsFromUserByState(username, true))
                .thenReturn(createListWithState(true));
        Mockito.when(repository.findFriendsToUserByState(username, true))
                .thenReturn(createListWithState(true));
        Mockito.when(repository.findFriendsFromUserByState(username, false))
                .thenReturn(createListWithState(false));
        Mockito.when(repository.findFriendsToUserByState(username, false))
                .thenReturn(createListWithState(false));
        Mockito.when(repository.findById(Mockito.anyLong()))
                .thenReturn(friendFactory.createEntity());
    }

    private List<Relationship> createListWithState(boolean state) {
        List<Relationship> relationships = friendFactory.createList(count);
        relationships.forEach(x -> x.setState(state));

        return relationships;
    }
}
