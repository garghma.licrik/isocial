package com.isocial.services;

import com.isocial.models.Hobby;
import com.isocial.repositories.HobbyRepository;
import com.isocial.repositories.exception.RepositoryException;
import com.isocial.services.exeption.ServiceException;
import com.isocial.services.impl.HobbyServiceImpl;
import com.isocial.services.utils.HobbyFactory;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.mockito.Mockito;

public class TestHobbyService {
    private static final String EXCEPTION_MESSAGE = "No entity found for query";

    private final HobbyService service;
    private final HobbyRepository repository;
    private final HobbyFactory hobbyFactory;

    public TestHobbyService() {
        this.repository = Mockito.mock(HobbyRepository.class);
        this.service = new HobbyServiceImpl(this.repository);
        hobbyFactory = new HobbyFactory();
    }

    @Test
    public void test_foundGetById() throws Exception {
        Hobby testHobby = hobbyFactory.createEntity();
        Mockito.when(repository.findById(Mockito.anyLong())).thenReturn(testHobby);

        Hobby resultHobby = service.getHobbyById(Mockito.anyLong());

        Mockito.verify(repository, Mockito.times(1)).findById(Mockito.anyLong());
        Assertions.assertEquals(testHobby, resultHobby);
    }

    @Test
    public void test_removeById() throws Exception {
        Hobby testHobby = hobbyFactory.createEntity();
        Mockito.when(repository.findById(Mockito.anyLong())).thenReturn(testHobby);

        service.removeById(Mockito.anyLong());

        Mockito.verify(repository, Mockito.times(1)).findById(Mockito.anyLong());
        Mockito.verify(repository, Mockito.times(1)).delete(Mockito.any(Hobby.class));
    }

    @Test
    public void test_insertNewHobby_withoutException() throws Exception {
        Hobby testHobby = hobbyFactory.createEntity();
        testHobby.setName("test");

        Mockito.when(repository.findByName(Mockito.anyString()))
                .thenThrow(new RepositoryException(EXCEPTION_MESSAGE));

        service.insertNewHobby(testHobby);

        Mockito.verify(repository, Mockito.times(1)).findByName(Mockito.anyString());
        Mockito.verify(repository, Mockito.times(1)).insert(testHobby);
    }

    @Test
    public void test_insertNewHobby_withException() throws Exception {
        String errorMessage = "";
        Hobby testHobby = hobbyFactory.createEntity();
        testHobby.setName("test");
        Mockito.when(repository.findByName(Mockito.anyString())).thenReturn(testHobby);

        try {
            service.insertNewHobby(testHobby);
        } catch (ServiceException exception) {
            errorMessage = exception.getMessage();
        }

        Assertions.assertEquals(errorMessage, "Hobby with " + testHobby.getName() +" name have database");
        Mockito.verify(repository, Mockito.times(1)).findByName(Mockito.anyString());
        Mockito.verify(repository, Mockito.never()).insert(testHobby);
    }

    @Test
    public void test_updateHobby() throws Exception {
        Hobby testHobby = hobbyFactory.createEntity();
        testHobby.setName("test name");
        Mockito.when(repository.findById(Mockito.anyLong())).thenReturn(testHobby);
        Mockito.when(repository.update(Mockito.any(Hobby.class)))
                .thenAnswer(i -> i.getArguments()[0]);

        Hobby resultHobby = service.updateHobby(Mockito.anyLong(), testHobby);

        Assertions.assertEquals(testHobby, resultHobby);
        Mockito.verify(repository, Mockito.times(1)).findById(Mockito.anyLong());
        Mockito.verify(repository, Mockito.times(1)).update(Mockito.any(Hobby.class));
    }
}
