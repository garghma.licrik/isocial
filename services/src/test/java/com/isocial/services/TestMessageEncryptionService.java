package com.isocial.services;

import com.isocial.models.User;
import com.isocial.services.impl.MessageEncryptionImpl;
import com.isocial.services.utils.UserFactory;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import java.util.ArrayList;
import java.util.List;

public class TestMessageEncryptionService {
    private final MessageEncryption service;
    private final UserFactory userFactory = new UserFactory();

    public TestMessageEncryptionService() {
        this.service = new MessageEncryptionImpl();
    }

    @Test
    public void test_encodingMessage() throws Exception {
        String message = "Hello";
        User first = userFactory.createEntity();
        User second = userFactory.createEntity();

        first.setId((long) 5);
        second.setId((long) 7);
        first.setUsername("first");
        second.setUsername("second");

        List<User> testList = new ArrayList<>();
        testList.add(first);
        testList.add(second);

        service.setCurrentDialogMembers(testList);
        String encoding = service.encoding(message);
        String decoding = service.decoding(encoding);

        Assertions.assertEquals(message, decoding);
    }

    @Test
    public void test_encodingMessage_notSortArray() throws Exception {
        String message = "Hello";
        User first = userFactory.createEntity();
        User second = userFactory.createEntity();

        first.setId((long) 5);
        second.setId((long) 7);
        first.setUsername("first");
        second.setUsername("second");

        List<User> testList = new ArrayList<>();
        testList.add(second);
        testList.add(first);

        service.setCurrentDialogMembers(testList);
        String encoding = service.encoding(message);
        String decoding = service.decoding(encoding);

        Assertions.assertEquals(message, decoding);
    }
}
