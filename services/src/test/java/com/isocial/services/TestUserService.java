package com.isocial.services;

import com.isocial.models.Post;
import com.isocial.models.User;
import com.isocial.repositories.UserRepository;
import com.isocial.repositories.exception.RepositoryException;
import com.isocial.services.exeption.ServiceException;
import com.isocial.services.impl.UserServiceImpl;
import com.isocial.services.utils.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.List;

public class TestUserService {
    private static final String USERNAME = "username";
    private static final String EXCEPTION_MESSAGE = "No entity found for query";
    private static final String BAD_EXCEPTION_MESSAGE = "No entity found for query bad";
    private static final int COUNT = 10;

    private final UserFactory userFactory;
    private final PostFactory postFactory;
    private final DialogMemberFactory dialogMemberFactory;
    private final CommunityMemberFactory communityMemberFactory;
    private final UserRepository userRepository;
    private final JwtProvider jwtProvider;
    private final PasswordEncoder passwordEncoder;
    private final UserService service;

    private User testUser;

    public TestUserService() {
        this.userRepository = Mockito.mock(UserRepository.class);
        this.passwordEncoder = Mockito.mock(PasswordEncoder.class);
        this.jwtProvider = Mockito.mock(JwtProvider.class);

        this.service = new UserServiceImpl(
                this.userRepository,
                this.jwtProvider,
                this.passwordEncoder);

        this.userFactory = new UserFactory();
        this.postFactory = new PostFactory();
        this.dialogMemberFactory = new DialogMemberFactory();
        this.communityMemberFactory = new CommunityMemberFactory();
    }

    @BeforeEach
    public void baseTestAction() {
        this.setTestUser();

        Mockito.when(userRepository.foundByUsernameWithPosts(Mockito.anyString()))
                .thenReturn(this.testUser);
        Mockito.when(userRepository.foundByUsername(Mockito.anyString()))
                .thenReturn(this.testUser);
        Mockito.when(userRepository.foundByUsernameWithDialogs(Mockito.anyString()))
                .thenReturn(this.testUser);
        Mockito.when(userRepository.foundByUsernameWithHobbies(Mockito.anyString()))
                .thenReturn(this.testUser);
        Mockito.when(userRepository.findById(Mockito.anyLong()))
                .thenReturn(this.testUser);
        Mockito.when(userRepository.findAll())
                .thenReturn(userFactory.createList(COUNT));
        Mockito.when(userRepository.foundByCity(Mockito.anyString()))
                .thenReturn(userFactory.createList(COUNT));
        Mockito.when(userRepository.update(Mockito.any(User.class)))
                .thenAnswer(i -> i.getArguments()[0]);

        Mockito.when(jwtProvider.createToken(USERNAME, "ADMIN"))
                .thenReturn(USERNAME);
        Mockito.when(passwordEncoder.matches(USERNAME, USERNAME)).thenReturn(true);
    }

    @Test
    public void testGetAllUsers_WithoutException() {
        List<User> resultList = service.getAllUsers();

        Mockito.verify(userRepository, Mockito.times(1)).findAll();
        Assertions.assertEquals(resultList.size(), COUNT);
    }

    @Test
    public void testFoundByUsername_WithoutError() {
        User resultUser = service.foundByUsername(USERNAME);

        Mockito.verify(userRepository, Mockito.times(1))
                .foundByUsername(USERNAME);
        Assertions.assertEquals(testUser, resultUser);
    }

    @Test
    public void testFoundByUsername_WithRepositoryException() {
        Mockito.when(userRepository.foundByUsername(Mockito.anyString()))
                .thenThrow(new RepositoryException(EXCEPTION_MESSAGE));

        User user = service.foundByUsername(USERNAME);

        Mockito.verify(userRepository, Mockito.times(1))
                .foundByUsername(USERNAME);
        Assertions.assertNull(user);
    }

    @Test
    public void testFoundByUsername_ThrowServiceException() {
        String messageException = "";
        Mockito.when(userRepository.foundByUsername(Mockito.anyString()))
                .thenThrow(new RepositoryException(BAD_EXCEPTION_MESSAGE));

        try {
            User user = service.foundByUsername(StaticConstant.TEST_STRING);
        } catch (ServiceException ex) {
            messageException = ex.getMessage();
        }

        Assertions.assertEquals(messageException, BAD_EXCEPTION_MESSAGE);
    }

    @Test
    public void testGetWithPosts_WithoutError() {
        User resultUser = service.getWithPosts(USERNAME);

        Mockito.verify(userRepository, Mockito.times(1))
                .foundByUsernameWithPosts(USERNAME);
        Assertions.assertEquals(testUser, resultUser);
        Assertions.assertArrayEquals(testUser.getPosts().toArray(), resultUser.getPosts().toArray());
    }

    @Test
    public void testGetWithPosts_WithRepositoryException() {
        Mockito.when(userRepository.foundByUsernameWithPosts(USERNAME))
                .thenThrow(new RepositoryException(EXCEPTION_MESSAGE));

        User resultUser = service.getWithPosts(USERNAME);

        Mockito.verify(userRepository, Mockito.times(1))
                .foundByUsernameWithPosts(USERNAME);
        Assertions.assertEquals(testUser, resultUser);
    }

    @Test
    public void testGetWithPosts_ThrowServiceException() {
        String exceptionMessage = StaticConstant.EXCEPTION_MESSAGE + " " + StaticConstant.TEST_STRING;
        Mockito.when(userRepository.foundByUsernameWithPosts(StaticConstant.TEST_STRING))
                .thenThrow(new RepositoryException(exceptionMessage));
        try {
            User user = service.getWithPosts(StaticConstant.TEST_STRING);
        } catch (ServiceException ex) {
            Assertions.assertEquals(ex.getMessage(), exceptionMessage);
        }
    }

    @Test
    public void testGetWithDialogs_WithoutError() throws Exception {
        User testUser = userFactory.createEntity();
        testUser.setDialogMembers(dialogMemberFactory.createList(COUNT));
        Mockito.when(userRepository.foundByUsernameWithDialogs(StaticConstant.TEST_STRING)).thenReturn(testUser);

        User resultUser = service.getWithDialogs(StaticConstant.TEST_STRING);

        Mockito.verify(userRepository, Mockito.times(1))
                .foundByUsernameWithDialogs(StaticConstant.TEST_STRING);
        Assertions.assertEquals(testUser, resultUser);
        Assertions.assertArrayEquals(testUser.getDialogMembers().toArray(), resultUser.getDialogMembers().toArray());
    }

    @Test
    public void testGetWithDialogs_WithRepositoryException() {
        Mockito.when(userRepository.foundByUsernameWithDialogs(USERNAME))
                .thenThrow(new RepositoryException(EXCEPTION_MESSAGE));

        User resultUser = service.getWithDialogs(USERNAME);

        Mockito.verify(userRepository, Mockito.times(1))
                .foundByUsernameWithDialogs(USERNAME);
        Assertions.assertEquals(resultUser, testUser);
    }

    @Test
    public void testGetWithDialogs_ThrowServiceException() {
        String exceptionMessage = StaticConstant.EXCEPTION_MESSAGE + " " + StaticConstant.TEST_STRING;
        Mockito.when(userRepository.foundByUsernameWithDialogs(StaticConstant.TEST_STRING))
                .thenThrow(new RepositoryException(exceptionMessage));
        try {
            User user = service.getWithDialogs(StaticConstant.TEST_STRING);
        } catch (ServiceException ex) {
            Assertions.assertEquals(ex.getMessage(), exceptionMessage);
        }
    }

    @Test
    public void testGetWithCommunities_WithoutError() throws Exception {
        User testUser = userFactory.createEntity();
        testUser.setCommunityMembers(communityMemberFactory.createList(COUNT));
        Mockito.when(userRepository.foundByUsernameWithCommunities(StaticConstant.TEST_STRING))
                .thenReturn(testUser);

        User resultUser = service.getWithCommunities(StaticConstant.TEST_STRING);

        Mockito.verify(userRepository, Mockito.times(1))
                .foundByUsernameWithCommunities(StaticConstant.TEST_STRING);
        Assertions.assertEquals(testUser, resultUser);
        Assertions.assertArrayEquals(testUser.getCommunityMembers().toArray(), resultUser.getCommunityMembers().toArray());
    }

    @Test
    public void testGetWithCommunities_WithRepositoryException() {
        Mockito.when(userRepository.foundByUsernameWithCommunities(USERNAME))
                .thenThrow(new RepositoryException(EXCEPTION_MESSAGE));

        User resultUser = service.getWithCommunities(USERNAME);

        Mockito.verify(userRepository, Mockito.times(1))
                .foundByUsernameWithCommunities(USERNAME);
        Assertions.assertEquals(testUser, resultUser);
    }

    @Test
    public void testGetWithCommunities_ThrowServiceException() {
        String exceptionMessage = StaticConstant.EXCEPTION_MESSAGE + " " + StaticConstant.TEST_STRING;
        Mockito.when(userRepository.foundByUsernameWithCommunities(StaticConstant.TEST_STRING))
                .thenThrow(new RepositoryException(exceptionMessage));
        try {
            User user = service.getWithCommunities(StaticConstant.TEST_STRING);
        } catch (ServiceException ex) {
            Assertions.assertEquals(ex.getMessage(), exceptionMessage);
        }
    }

    @Test
    public void testCreatePost_WithoutException() throws Exception {
        User testUser = userFactory.createEntity();
        testUser.setPosts(postFactory.createList(COUNT));
        Post testPost = postFactory.createEntity();

        Mockito.when(userRepository.foundByUsernameWithPosts(StaticConstant.TEST_STRING))
                .thenReturn(testUser);
        Mockito.when(userRepository.update(Mockito.any())).thenReturn(testUser);

        User resultUser = service.createPost(StaticConstant.TEST_STRING, testPost);

        Mockito.verify(userRepository, Mockito.times(1))
                .foundByUsernameWithPosts(StaticConstant.TEST_STRING);
        Mockito.verify(userRepository, Mockito.times(1))
                .update(Mockito.any());
        Assertions.assertEquals(testUser, resultUser);
    }

    private void setTestUser() {
        this.testUser = userFactory.createEntity();
        this.testUser.setId((long) 0);
        this.testUser.setPosts(postFactory.createList(COUNT));
        this.testUser.setUsername(USERNAME);
        this.testUser.setDialogMembers(dialogMemberFactory.createList(COUNT));
        this.testUser.setCommunityMembers(communityMemberFactory.createList(COUNT));
    }
}
