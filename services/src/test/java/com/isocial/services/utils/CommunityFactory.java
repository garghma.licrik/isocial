package com.isocial.services.utils;

import com.isocial.models.Community;

public class CommunityFactory extends EntityFactory<Community> {
    public CommunityFactory() {
        super(Community.class);
    }
}
