package com.isocial.services.utils;

import com.isocial.models.CommunityMember;

public class CommunityMemberFactory extends EntityFactory<CommunityMember> {
    public CommunityMemberFactory() {
        super(CommunityMember.class);
    }
}
