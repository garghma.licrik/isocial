package com.isocial.services.utils;

import com.isocial.models.Dialog;

public class DialogFactory extends EntityFactory<Dialog> {
    public DialogFactory() {
        super(Dialog.class);
    }
}
