package com.isocial.services.utils;

import com.isocial.models.DialogMember;

public class DialogMemberFactory extends EntityFactory<DialogMember> {
    public DialogMemberFactory() {
        super(DialogMember.class);
    }
}
