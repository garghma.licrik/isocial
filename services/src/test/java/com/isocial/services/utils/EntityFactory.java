package com.isocial.services.utils;

import com.isocial.models.AEntity;

import javax.persistence.Basic;
import java.util.ArrayList;
import java.util.List;

public abstract class EntityFactory<E extends AEntity> {
    private Class<E> type;

    EntityFactory(Class<E> type) {
        this.type = type;
    }

    public List<E> createList(int count) {
        List<E> list = new ArrayList<>();

        for (int i = 0; i < count; i++) {
            E entity = createEntity();
            entity.setId((long) i);
            list.add(entity);
        }

        return list;
    }

    public E createEntity() {
        try {
            return type.newInstance();
        } catch (Exception exception) {
            return null;
        }
    }
}
