package com.isocial.services.utils;

import com.isocial.models.Hobby;

public class HobbyFactory extends EntityFactory<Hobby> {
    public HobbyFactory() {
        super(Hobby.class);
    }
}
