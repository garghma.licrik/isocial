package com.isocial.services.utils;

import com.isocial.models.Message;

public class MessageFactory extends EntityFactory<Message> {
    public MessageFactory() {
        super(Message.class);
    }
}
