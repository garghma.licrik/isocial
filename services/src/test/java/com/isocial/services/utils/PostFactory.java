package com.isocial.services.utils;

import com.isocial.models.Post;

public class PostFactory extends EntityFactory<Post> {
    public PostFactory() {
        super(Post.class);
    }
}
