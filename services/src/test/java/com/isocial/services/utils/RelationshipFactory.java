package com.isocial.services.utils;

import com.isocial.models.Relationship;

public class RelationshipFactory extends EntityFactory<Relationship> {
    public RelationshipFactory() {
        super(Relationship.class);
    }
}
