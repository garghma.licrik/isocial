package com.isocial.services.utils;

public class StaticConstant {
    public static final String TEST_STRING = "test string";
    public static final String EXCEPTION_MESSAGE = "";
    public static final String BAD_EXCEPTION_MESSAGE = EXCEPTION_MESSAGE + " " + TEST_STRING;
}
