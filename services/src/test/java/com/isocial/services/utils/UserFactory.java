package com.isocial.services.utils;

import com.isocial.models.User;
import org.springframework.stereotype.Component;

public class UserFactory extends EntityFactory<User> {
    public UserFactory() {
        super(User.class);
    }
}
